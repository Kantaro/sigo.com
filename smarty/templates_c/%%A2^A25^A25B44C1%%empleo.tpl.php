<?php /* Smarty version 2.6.9, created on 2018-09-26 15:12:47
         compiled from empleo.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_revista.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<title><?php echo $this->_tpl_vars['accion']; ?>
 | Sigo Venezuela</title>
<meta name="Title" content="Sigo S.A." />
<meta name="Author" content="Alejandro D&iacute;az http://diazcreativos.net.ve/" />
<meta name="Subject" content="Sigo S.A." />
<meta name="Description" content="<?php echo $this->_tpl_vars['descripcion']; ?>
" /> 
<meta name="Keywords" content="<?php echo $this->_tpl_vars['claves']; ?>
" />

<meta name="Generator" content="DreamWeaver" /> 
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="7 days" /> 
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />

<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico" />
<script src="/js/validar.js" type="text/javascript"></script>
<link href="/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />
<link rel="stylesheet" href="/css/jshowoff.css" type="text/css" media="screen, projection" />
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery.skitter.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jshowoff.min.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.js"></script>
<link rel="stylesheet" type="text/css" href="/css/skin.css" />
<link href="/css/styles.css" type="text/css" media="all" rel="stylesheet" />
<link href="/dropdown/menu.css" type="text/css" media="all" rel="stylesheet" />
<?php echo '
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery(\'#mycarousel\').jcarousel({
		vertical: true,
        auto: 4,
		easing: \'easeOutBack\',
		animation: \'slow\',
		scroll: 3,
        wrap: \'circular\',
        initCallback: mycarousel_initCallback,
		itemLoadCallback: itemLoadCallbackFunction
    });
});

$(function(){
    // Skitter
    $(\'.box_skitter_large\').skitter();
    
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push([\'_setAccount\', \'UA-6623041-23\']);
  _gaq.push([\'_trackPageview\']);

  (function() {
    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
    ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
'; ?>

<!-- InstanceBeginEditable name="head" -->
<?php echo '
<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=places&sensor=true"></script>

<script type="text/javascript" src="/js/jquery.lightbox-0.5.js"></script>
<link rel="stylesheet" type="text/css" href="/css/jquery.lightbox-0.5.css" media="screen" />
<script type="text/javascript">
$(function() {
	$(\'#gallery a\').lightBox();
});
</script>

'; ?>

<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('/imagenes/logo_twitter_color.png','/imagenes/logo_facebook_color.png','/imagenes/logo_youtube_color.png')">
<div id="main_principal">
<div id="encabezado2">
        <div id="logopequeno"><a href="index.php" title="Home"><img src="/imagenes/logotipo_<?php echo $this->_tpl_vars['logo']; ?>
.png" width="154" height="144" border="0" /></a></div> 
        <div id="botonera_A">
            <ul id="menu">
                    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_A']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <li><a href="contenido.php?cont=<?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['id_cat']; ?>
#next"><?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['nombre_cat']; ?>
</a>
                        <?php if ($this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces'] != ""): ?>
                        <div class="dropdown_3columns">
                            <?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
                                <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=<?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['id_cat']; ?>
&sub=<?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['id_sub']; ?>
#next">
                                <?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['nombre_sub']; ?>
</a> -->
                                <?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['nombre_sub']; ?>

                                </div>
                            <?php endfor; endif; ?>
                        </div>
                        <?php endif; ?>
                    </li>
                	<?php endfor; endif; ?>
            </ul>
        </div>
        
    </div>
  
  <a id="next"></a>
  <!-- InstanceBeginEditable name="contenido" --><br />
  
  <div class="entradas2">
  <div id="modulo_empleo">
  		<div><img src="/imagenes/carreer.jpg" width="936" height="376" /></div><br />
        <div class="encabezado_dos"><div class="membrete titulo"><?php echo $this->_tpl_vars['accion']; ?>
</div></div>
  </div>
    <div id="formulario_contacto">
   	  <form action="" method="post" enctype="multipart/form-data" name="form2" id="form2"> 
<div id="etiquetas">
            	<div class="etiqueta2">Nombre: </div>
                <div class="campo"><input value="<?php echo $this->_tpl_vars['nombre']; ?>
" type="text" name="nombre" id="nombre" onkeypress="javascript: return validarletras(event);" /></div>
                <div class="etiqueta2">Apellido: </div>
                <div class="campo"><input value="<?php echo $this->_tpl_vars['nombre']; ?>
" type="text" name="nombre" id="nombre" onkeypress="javascript: return validarletras(event);" /></div>
<div class="etiqueta2">Email:</div>
                <div class="campo"><input onkeypress="javascript: return validarcorreo(event);" value="<?php echo $this->_tpl_vars['email']; ?>
" type="text" name="email" id="email" /></div>
<div class="etiqueta2">Tel&eacute;fono:</div>
                <div class="campo"><input onkeypress="javascript: return validarnum(event);" value="<?php echo $this->_tpl_vars['telefono']; ?>
" type="text" name="telefono" id="telefono" /></div>
                <div class="etiqueta2">Curriculum:</div>
                <div class="campo">
                  <input type="file" name="fileField" id="fileField" />
                </div>
      <div class="etiqueta">Mensaje:</div>
                <div class="campo2">
                  <textarea name="comentario" rows="6" id="mensaje"><?php echo $this->_tpl_vars['comentario']; ?>
</textarea>
        </div>
        </div> 
    	  	<div id="boton_enviar"><input name="envio" type="hidden" id="envio" value="Enviar" />
   	     	<a title="Enviar Formulario" href="#" onclick="javascripts: return validarformulario();"><img src="/imagenes/enviar.png" width="95" height="36" border="0" class="opacidad" /></a> </div>
   	  </form>
    </div>
              
  </div>
  <!-- InstanceEndEditable -->
  <div id="menu_categorias">
  <div id="redes2">
   		<div class="redes_icono2"> <a href="http://twitter.com/sigosa"><img src="/imagenes/logo_twitter_gris.png" width="46" height="46" border="0" id="Image1" onmouseover="MM_swapImage('Image1','','/imagenes/logo_twitter_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.facebook.com/SigoVenezuela"><img src="/imagenes/logo_face_gris.png" width="46" height="46" border="0" id="Image2" onmouseover="MM_swapImage('Image2','','/imagenes/logo_facebook_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.youtube.com/SigoVenezuela"><img src="/imagenes/logo_youtube_gris.png" width="46" height="46" border="0" id="Image3" onmouseover="MM_swapImage('Image3','','/imagenes/logo_youtube_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
</div>
		
     <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_C']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<div class="modulo">
            <div><a href="#" title="<?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['nombre_cat']; ?>
">
            <span class="titulo4"><?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['nombre_cat']; ?>
</span></a></div>
            <?php if ($this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces'] != ""): ?>
                <?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
                <div <?php if ($this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['id_cat'] == '16'): ?>class="submodulos2"<?php else: ?>class="submodulos"<?php endif; ?>>
                <a href="contenido_sub.php?cont=<?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['id_cat']; ?>
&sub=<?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['id_sub']; ?>
#next" title="<?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['etiqueta_sub']; ?>
" class="enlace_top">&bull; <?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['nombre_sub']; ?>
</a></div>
                <?php endfor; endif; ?>
            <?php endif; ?>
        </div>
     <?php endfor; endif; ?>
	
  </div>
</div>
<div class="clear"></div>
 
<div id="panel_pie">
    <div id="pie">
    <div id="botonera">
    	<div class="boton"><a href="index.php">Home</a></div>
        <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_B']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
            <div class="boton"><a href="contenido.php?cont=<?php echo $this->_tpl_vars['enlaces_B'][$this->_sections['i']['index']]['id_cat']; ?>
#next"><?php echo $this->_tpl_vars['enlaces_B'][$this->_sections['i']['index']]['nombre_cat']; ?>
</a></div>
        <?php endfor; endif; ?>
        <div class="boton"><a title="Contactos"  href="contacto.php">Cont&aacute;ctanos</a></div>
        <div class="boton_less">&nbsp;</div>
    </div>
    <div id="logominimo"><img src="/imagenes/loguito.png" width="50" height="45" /></div>
    <div id="copy"> | SIGO S.A. &reg; 2016 RIF:J-08003048-6</div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>