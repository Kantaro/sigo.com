<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>{$accion} | Sigo Venezuela</title>
<meta name="Title" content="Sigo S.A." />
<meta name="Author" content="Alejandro Díaz http://diazcreativos.net.ve/" />
<meta name="Subject" content="Sigo S.A." />
<meta name="Description" content="{$descripcion}" />
<meta name="Keywords" content="{$claves}" />

<meta name="Generator" content="DreamWeaver" />
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="7 days" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />

<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico" />
<script src="/js/validar.js" type="text/javascript"></script>
<link href="/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />
<link rel="stylesheet" href="/css/jshowoff.css" type="text/css" media="screen, projection" />
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery.skitter.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jshowoff.min.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.js"></script>
<link rel="stylesheet" type="text/css" href="/css/skin.css" />
<link href="/css/styles.css" type="text/css" media="all" rel="stylesheet" />
<link href="/dropdown/menu.css" type="text/css" media="all" rel="stylesheet" />
{literal}
<script type="text/javascript">

$(document).ready(function() {
  $('#puntos_enviar').click(function(e){
    e.preventDefault()
    $('#error_cliente').html("")
    cedula = $('#IdDocument').val()
    if (cedula != ''){
      $('#spinner').show()
      $('#tr_contenido').hide()
      $('#tabla_clientes tbody').html(" ")
      $('#tabla_cuentas tbody').html(" ")
      $.ajax({
        url: "sigojson.php",
        type: "GET",
        data: "cedula="+cedula,
        success: function (data){
          $('#spinner').hide()
          try{
            datos = $.parseJSON(data)
            $('#tr_contenido').show()
            $.each(datos.Customer, function(i, v){
              $('#tabla_clientes tbody').append("<tr><td class='titulo_alt'>"+v.Fullname+"</td><td class='titulo_alt'>"+v.cod_sigoclub+"</td></tr>")
            })
            $.each(datos.AccountBalance, function(i, v){
              $('#tabla_cuentas tbody').append("<tr><td class='titulo_alt'>"+v.tipocuenta+"</td><td class='titulo_alt'>"+v.saldo+"</td></tr>")
            })
          }
          catch(e){
             $('#error_cliente').html("Disculpe: " + e)
          }
        },
        error: function (e){
         console.log ("error: ", e)
        }
      })
    } //:~ if cedula
  })
});


</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=places&sensor=true"></script>
{/literal}

</head>

<body onload="MM_preloadImages('/imagenes/logo_twitter_color.png','/imagenes/logo_facebook_color.png','/imagenes/logo_youtube_color.png')">
<div id="main_principal">
<div id="encabezado2">
        <div id="logopequeno"><a href="index.php" title="Home"><img src="/imagenes/logotipo_{$logo}.png" width="154" height="144" border="0" /></a></div>
        <div id="botonera_A">
            <ul id="menu">
                    {section name=i loop=$enlaces_A}
                    <li><a href="contenido.php?cont={$enlaces_A[i].id_cat}#next">{$enlaces_A[i].nombre_cat}</a>
                        {if $enlaces_A[i].enlaces neq ""}
                        <div class="dropdown_3columns">
                            {section name=j loop=$enlaces_A[i].enlaces}
                                <div class="col_1"><a href="contenido_sub.php?cont={$enlaces_A[i].id_cat}&sub={$enlaces_A[i].enlaces[j].id_sub}#next">
                                {$enlaces_A[i].enlaces[j].nombre_sub}</a>
                                </div>
                            {/section}
                        </div>
                        {/if}
                    </li>
                	{/section}
            </ul>
        </div>
</div>
<a id="next"></a><br />

  <div id="panel_sigoclub">
  		<div id="formulario_sigoclub">
        	<form>
        <table width="90%" border="0" align="center" cellpadding="4" cellspacing="0">
              <tr>
                <td height="32" colspan="2" align="center" valign="top" class="titulo_alt">Cedula:
                  <input name="IdDocument" onkeypress="javascripts: return validarnum(event)" type="text" class="componentes" id="IdDocument" size="25" maxlength="12" />
                <input name="envio" type="submit" class="componentes" id="puntos_enviar" value="Verificar" />
                Introduce tu n&uacute;mero de c&eacute;dula</td>
              </tr>
              <tr id="spinner" style="display:none">
                <td height="32" colspan="2" align="center" valign="top" class="titulo_alt"><img  style="" src="/imagenes/spinner.gif" width="64" height="64" /></td>
               </tr>

              <tr id="tr_contenido" style="display:none">
                <td width="50%" align="left" valign="top" class="titulo">
                  <table id="tabla_clientes" width="100%" border="0" cellspacing="0" cellpadding="6">
                    <thead>
                 <tr class="titulo2">
                    <td width="60%">Nombre</td>
                    <td width="40%">C&oacute;digo Sigo Club</td>
                  </tr>

                </thead>
                <tbody>
                </tbody>
                </table>
              </td>
                <td width="50%" align="left" valign="top" class="normalContenido">
                  <table id="tabla_cuentas" width="100%" border="0" cellspacing="0" cellpadding="6">
                    <thead>
                  <tr class="titulo2">
                    <td>Tipo de Cuenta</td>
                    <td>Saldo</td>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
              </td>
              </tr>
              {$mensaje}
            </table>
      </form>
      <center><span id='error_cliente' style='color: red'></span></center>
        </div>
  </div>

<div id="sigoclub_contenido">
<p><span class="titulo"><img src="/imagenes/cuadrito_09b.jpg" width="11" height="12" />  ¿Qué es Sigo Club?</span><br />
Sigo Club es un programa de fidelidad innovador hecho a la medida de Sigo y sus clientes naturales, que ofrece la posibilidad de acumular puntos cada vez que hagas tus compras. También obtienes beneficios automáticos en una gran cantidad de empresas relacionadas a Sigo Club. 
</p>
</div>

  <div id="menu_categorias">
    <div id="redes2">
   		<div class="redes_icono2"> <a href="http://twitter.com/sigosa"><img src="/imagenes/logo_twitter_gris.png" width="46" height="46" border="0" id="Image1" onmouseover="MM_swapImage('Image1','','/imagenes/logo_twitter_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.facebook.com/SigoVenezuela"><img src="/imagenes/logo_face_gris.png" width="46" height="46" border="0" id="Image2" onmouseover="MM_swapImage('Image2','','/imagenes/logo_facebook_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.youtube.com/SigoVenezuela"><img src="/imagenes/logo_youtube_gris.png" width="46" height="46" border="0" id="Image3" onmouseover="MM_swapImage('Image3','','/imagenes/logo_youtube_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
</div>

    {section name=i loop=$enlaces_C}
		<div class="modulo">
            <div><a href="#" title="{$enlaces_C[i].nombre_cat}">
            <span class="titulo4">{$enlaces_C[i].nombre_cat}</span></a></div>
            {if $enlaces_C[i].enlaces neq ""}
                {section name=j loop=$enlaces_C[i].enlaces}
                <div {if $enlaces_C[i].id_cat eq "16"}class="submodulos2"{else}class="submodulos"{/if}>
                <a href="contenido_sub.php?cont={$enlaces_C[i].id_cat}&sub={$enlaces_C[i].enlaces[j].id_sub}#next" title="{$enlaces_C[i].enlaces[j].etiqueta_sub}" class="enlace_top">&bull; {$enlaces_C[i].enlaces[j].nombre_sub}</a></div>
                {/section}
            {/if}
        </div>
     {/section}

</div>
</div>
<div class="clear"></div>

<div id="panel_pie">
    <div id="pie">
    <div id="botonera">
    	<div class="boton"><a href="index.php">Home</a></div>
        {section name=i loop=$enlaces_B}
            <div class="boton"><a href="contenido.php?cont={$enlaces_B[i].id_cat}#next">{$enlaces_B[i].nombre_cat}</a></div>
        {/section}
        <div class="boton"><a href="#">Cont&aacute;ctanos</a></div>
        <div class="boton_less">&nbsp;</div>
    </div>
    <div id="logominimo"><img src="/imagenes/loguito.png" width="50" height="45" /></div>
    <div id="copy"> | SIGO S.A. ® 2016 RIF:J-08003048-6</div>
  </div>
</div>
</body>
</html>
