<?php
/* header para Smarty */
require('config/setup.php');
$smarty = new objeto_smarty;
/*  Fin header para Smarty */

include_once ("config/class.login.php");
include_once ("config/class.link.php");
include_once ("config/class.contenido.php");
include_once ("config/class.galeria.php");
include_once ("config/class.mapa.php");
include_once ("config/class.categoria.php");
include_once ("config/class.publicidad.php");
include_once("config/conexion.inc.php");
require('smarty/libs/SmartyPaginate.class.php');
include_once ("config/class.banner.php");

session_start();
if(!isset($acceso))
	$acceso = new Auth;
if ($_POST){
	if ($_POST['enviar'] == "Login"){
		$acceso->asignar_consulta($_POST['login'],$_POST['clave']);
		$acceso->login2($acceso->login, $acceso->password);
	};
}
if ($_GET){
	if ($_GET['enviar'] == "Logout")
		$acceso->logout();
}

if($_GET['cont']==18 && $_GET['sub']==114){
	header("location: sigoclub.php");
	exit();
}else if($_GET['cont']==17 && $_GET['sub']==115){
	header("location: empleo.php");
	exit();
}else if($_GET['cont']==19 && $_GET['sub']==122){
	header("location: eventos.php");
	exit();
}else if($_GET['cont']==18 && $_GET['sub']==112){
	header("location: eventos.php");
	exit();
}

SmartyPaginate::disconnect();
// required connect
SmartyPaginate::connect();
// set items per page
SmartyPaginate::setLimit(10);
SmartyPaginate::setPrevText('Anterior');
SmartyPaginate::setNextText('Siguiente');
SmartyPaginate::setFirstText('Primer');
SmartyPaginate::setLastText('�ltimo');

if(isset($_GET['cont'])){
	if($_GET['cont']!=$_SESSION['contenido_actual'])
		unset($_SESSION['buscar']);
	$_SESSION['contenido_actual']=$_GET['cont'];
}

$cont=$_SESSION['contenido_actual'];

SmartyPaginate::setUrl("contenido_sub.php?cat=$cat&sub=".$_GET['sub']);

if(!isset($banner))
	$banner= new Banner;
$banner->listar_banner_publica($cont);

if(!isset($link))
	$link= new Link;
$link->listar_link_menu("todo");
$link->mostrar_link_publico($cont);

if(!isset($enlaces_A))
	$enlaces_A= new Link();
$enlaces_A->listar_link_menu("arriba");

if(!isset($enlaces_B))
	$enlaces_B= new Link();
$enlaces_B->listar_link_menu("central");

if(!isset($enlaces_C))
	$enlaces_C= new Link();
$enlaces_C->listar_link_menu("abajo");

if(!isset($contenido))
	$contenido= new Contenido();

$contenido->listar_contenido_imagen($cont);
$nombresub=$contenido->get_subenlace($_GET['sub']);
$contenido->accion=$link->etiqueta." | ".$nombresub; 
$data=$contenido->listado;

if($contenido->mensaje!="si"){
	$mensaje2="<tr><td align='center' colspan='2' class='error'>No existen registros en esta secci�n</td></tr>";	
}

if(!isset($sublink))
	$sublink= new Link();
$sublink->cargar_sublink();

function get_db_results($_data) {
	SmartyPaginate::setTotal(count($_data));
	return array_slice($_data, SmartyPaginate::getCurrentIndex(),
		SmartyPaginate::getLimit());
}

SmartyPaginate::setUrl("contenido_sub.php?cont=".$_GET['cont']."&sub=".$_GET['sub']);

if(!isset($publicidad))
	if(!isset($publicidad))
	$publicidad= new Publicidad;
$publicidad->cargar_publicidad("Banner Izquierdo");
$smarty->assign("publicidad", $publicidad->listado);

if(!isset($publicidad2))
	$publicidad2= new Publicidad;
$publicidad2->cargar_publicidad("Banner Derecho");
$smarty->assign("publicidad2", $publicidad2->listado);

if(!isset($imagenes))
	$imagenes= new Galeria;
$imagenes->mostrar_imagenes3("contenido", $contenido->listado[0]['id_con']);

$mapa=new Mapa;
$mapa->listar_mapa_publica($contenido->listado[0]['id_con']);
if($mapa->mensaje!="si"){
	$mensaje="<tr><td align='center' class='error'>No hay punto en el mapa definido!</td></tr>";	
}

if(isset($mapa->listado) && $mapa->listado!=""){
	$i=1;
	$baches="[";
	foreach($mapa->listado as $valor => $indice){
		if($i!=1) $baches.=", ";
		$baches.="['".$mapa->listado[$valor]['id_map']."', ".$mapa->listado[$valor]['latitud_map'].", ".$mapa->listado[$valor]['longitud_map'].", ".$i."]";
		$i++;
	}
	$baches.= "];";
	
	$latitud=$mapa->listado[0]['latitud_map'];
	$longitud=$mapa->listado[0]['longitud_map'];
}

$mapas="<script>
      function initialize() {
        var mapOptions = {
          zoom: 16,
          center: new google.maps.LatLng($latitud, $longitud),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('map2'),
                                      mapOptions);

        setMarkers(map, beaches);
      }
	  
      var beaches = $baches

      function setMarkers(map, locations) {
  
        var image = new google.maps.MarkerImage('/imagenes/bandera_sigo.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            new google.maps.Size(119, 119),
            // The origin for this image is 0,0.
            new google.maps.Point(0,0),
            // The anchor for this image is the base of the flagpole at 0,119.
            new google.maps.Point(0, 119));
        var shadow = new google.maps.MarkerImage('/imagenes/bandera_sigo.png',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(119, 119),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 119));
            // Shapes define the clickable region of the icon.
            // The type defines an HTML &lt;area&gt; element 'poly' which
            // traces out a polygon as a series of X,Y points. The final
            // coordinate closes the poly by connecting to the first
            // coordinate.
        var shape = {
            coord: [1, 1, 1, 20, 18, 20, 18 , 1],
            type: 'poly'
        };
        for (var i = 0; i < locations.length; i++) {
          var beach = locations[i];
          var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
          var marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
              shadow: shadow,
              icon: image,
              shape: shape,
              title: beach[0],
              zIndex: beach[3]
          });
        }
      }
	  google.maps.event.addDomListener(window, 'load', initialize);
    </script>";
	
/* footer para Smarty */
$smarty->assign("categoria", $nombre);
$smarty->assign("mensaje2", $mensaje2);
$smarty->assign("mensaje", $mensaje);
$smarty->assign("mapas", $mapas);
$smarty->assign("logo", $acceso->logo);
$smarty->assign("accion", $contenido->accion);
$smarty->assign("claves", $link->claves);
$smarty->assign("descripcion", $link->descripcion);
$smarty->assign("cont", $cont);
$smarty->assign("enlaces", $link->listado);
$smarty->assign('enlaces_A',$enlaces_A->listado);
$smarty->assign('enlaces_B',$enlaces_B->listado);
$smarty->assign('enlaces_C',$enlaces_C->listado);
$smarty->assign("banner", $banner->listado);
$smarty->assign('subcategorias',$sublink->listado);
$smarty->assign('imagenes',$imagenes->listado);

if(isset($data) && $data!=""){
	$smarty->assign('contenido', get_db_results($data));
	// assign {$paginate} var
	SmartyPaginate::assign($smarty);
	// display results
}
$smarty->force_compile=true;
if($cont=="15" || $cont=="16" || $cont=="17" || $cont=="18" || $cont=="19" || $cont=="20")
	$smarty->display('contenido4.tpl');
else
	$smarty->display('contenido2.tpl');
/* Fin footer para Smarty */
?>