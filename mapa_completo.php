<?php
/* header para Smarty */
require('config/setup.php');
$smarty = new objeto_smarty;
/*  Fin header para Smarty */ 
include_once ("config/class.login.php");
include_once ("config/class.link.php");
include_once ("config/class.contenido.php");
include_once ("config/class.categoria.php");
include_once ("config/class.publicidad.php");
include_once ("config/class.banner.php");
include_once ("config/class.noticia.php");
include_once ("config/class.mapa.php");
include_once("config/conexion.inc.php");

if(!isset($acceso))
	$acceso = new Auth;
if ($_POST){
	if ($_POST['enviar'] == "Login"){
		$acceso->asignar_consulta($_POST['login'],$_POST['clave']);
		$acceso->login2($acceso->login, $acceso->password);
	};
}
if ($_GET){
	if ($_GET['enviar'] == "Logout")
		$acceso->logout();
}

if(isset($_GET['msg']) && $_GET['msg']==1){
	$mensaje="<tr><td align='center' colspan='2' class='error'>User session has expired! please enter again!</td></tr>";	
}else if(isset($_GET['msg']) && $_GET['msg']==2){
	$mensaje="<tr><td align='center' colspan='2' class='error'>You do not own privileges to enter this area!</td></tr>";	
}else if($acceso->mensaje!=""){
	$mensaje="<tr><td align='center' colspan='2' class='error'>$acceso->mensaje</td></tr>";
}

if(!isset($categoria))
	$categoria= new Categoria;
$categoria->listar_categoria_menu();
$smarty->assign("categorias", $categoria->listado);

if(!isset($link))
	$link= new Link;
$link->listar_link_menu("todo"); 
$link->mostrar_link_publico($content);

if(!isset($enlaces_A))
	$enlaces_A= new Link();
$enlaces_A->listar_link_menu("arriba");

if(!isset($enlaces_B))
	$enlaces_B= new Link();
$enlaces_B->listar_link_menu("central");

if(!isset($enlaces_C))
	$enlaces_C= new Link();
$enlaces_C->listar_link_menu("abajo");

//Modulo para busqueda y obtenci�n de resultados
$mapa=new Mapa;
$mapa->listar_mapa_publica2();
if($mapa->mensaje!="si"){
	$mensaje="<tr><td align='center' class='error'>No hay punto en el mapa definido!</td></tr>";	
}

//print_r($mapa->listado);

if(isset($mapa->listado) && $mapa->listado!=""){
	$i=1;
	$baches="[";
	foreach($mapa->listado as $valor => $indice){
		if($i!=1) $baches.=", ";
		$baches.="['".$mapa->listado[$valor]['nombre_con']."', ".$mapa->listado[$valor]['latitud_map'].", ".$mapa->listado[$valor]['longitud_map'].", ".$i.", '".$mapa->listado[$valor]['directorio_image']."']";
		$i++;
	}
	$baches.= "];";
	
	$latitud= 6.8828;
	$longitud= -66.228882;
}

$total=$i;

$mapas="<script>
      function initialize() {
        var mapOptions = {
          zoom: 6,
          center: new google.maps.LatLng(".$latitud.", ".$longitud."),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('map3'),
                                      mapOptions);

        setMarkers(map, beaches);
      }
      var beaches = ".$baches."

      function setMarkers(map, locations) {";
		
      for ($i = 0; $i < $total; $i++) {
			
	$mapas.="var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[".$i."][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[".$i."][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id=\"content\">'+
            '<div id=\"bodyContent\">'+
            '<p><img width=\"140\" alt=\"".$mapa->listado[$i]['nombre_con']."\" border=\"0\" src=\"/imagenes/mapa/".$mapa->listado[$i]['directorio_image']."\" class=\"opacidad fotos2\" />'+
			'<b>".$mapa->listado[$i]['nombre_con']."</b></p>'+
            '<p><a href=\"contenido.php?cont=".$mapa->listado[$i]['enlace_con']."\">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow".$i." = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[".$i."];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker".$i." = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker".$i.", 'click', function() {
			infowindow".$i.".open(map,marker".$i.");});";
      }
	  
	  $mapas.="} google.maps.event.addDomListener(window, 'load', initialize);
    </script>";

/* footer para Smarty */
$smarty->assign('nombre_uso',$_SESSION['nombre_temporal']);
$smarty->assign('apellido_uso',$_SESSION['apellido_temporal']);
$smarty->assign("logo", $acceso->logo);
$smarty->assign("mapas", $mapas);
$smarty->assign("accion", "Sucursales de Sigo en el territorio Venezolano");
$smarty->assign("descripcion", $link->descripcion);
$smarty->assign("claves", $link->claves);
$smarty->assign("mensaje2", $mensaje2);
$smarty->assign("mensaje", $mensaje);
$smarty->assign("enlaces", $link->listado);
$smarty->assign('enlaces_A',$enlaces_A->listado);
$smarty->assign('enlaces_B',$enlaces_B->listado);
$smarty->assign('enlaces_C',$enlaces_C->listado);
$smarty->assign('noticias',$noticia->listado);
$smarty->assign("banner", $banner->listado);
$smarty->assign('contenido', $contenido->listado);
$smarty->force_compile=true;
$smarty->display('mapa.tpl');
/* Fin footer para Smarty */
?>