129
a:4:{s:8:"template";a:1:{s:8:"mapa.tpl";b:1;}s:9:"timestamp";i:1562879294;s:7:"expires";i:1562882894;s:13:"cache_serials";a:0:{}}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Sucursales de Sigo en el territorio Venezolano | Sigo Venezuela</title>
<meta name="Title" content="Sigo S.A." />
<meta name="Author" content="Alejandro Díaz http://diazcreativos.net.ve/" />
<meta name="Subject" content="Sigo S.A." />
<meta name="Description" content="" /> 
<meta name="Keywords" content="" />

<meta name="Generator" content="DreamWeaver" /> 
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="7 days" /> 
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />

<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico" />
<script src="/js/validar.js" type="text/javascript"></script>
<link href="/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />
<link rel="stylesheet" href="/css/jshowoff.css" type="text/css" media="screen, projection" />
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery.skitter.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jshowoff.min.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.js"></script>
<link rel="stylesheet" type="text/css" href="/css/skin.css" />
<link href="/css/styles.css" type="text/css" media="all" rel="stylesheet" />
<link href="/dropdown/menu.css" type="text/css" media="all" rel="stylesheet" />

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
		vertical: true,
        auto: 4,
		easing: 'easeOutBack',
		animation: 'slow',
		scroll: 3,
        wrap: 'circular',
        initCallback: mycarousel_initCallback,
		itemLoadCallback: itemLoadCallbackFunction
    });
});

$(function(){
    // Skitter
    $('.box_skitter_large').skitter();
    
});
</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=places&sensor=true"></script>


</head>

<body onload="MM_preloadImages('/imagenes/logo_twitter_color.png','/imagenes/logo_facebook_color.png','/imagenes/logo_youtube_color.png')">
<div id="main_principal">
<div id="encabezado2">
        <div id="logopequeno"><a href="index.php" title="Home"><img src="/imagenes/logotipo_D.png" width="154" height="144" border="0" /></a></div>
        <div id="botonera_A">
            <ul id="menu">
                                        <li><a href="contenido.php?cont=1#next">Home</a>
                                            </li>
                	                    <li><a href="contenido.php?cont=2#next">HyperMarket</a>
                                                <div class="dropdown_3columns">
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=64#next">
                                Artículos del Hogar</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=65#next">
                                Bebés</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=66#next">
                                Bodegón</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=67#next">
                                Carnicería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=68#next">
                                Charcutería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=69#next">
                                Chucherías</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=70#next">
                                Computación</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=71#next">
                                Congelados</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=72#next">
                                Cuidado Personal</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=73#next">
                                Deportes</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=75#next">
                                Farmacia</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=74#next">
                                Electrodomésticos</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=76#next">
                                Ferretería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=77#next">
                                Frulever</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=78#next">
                                Lámparas</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=79#next">
                                Lencería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=80#next">
                                Limpieza</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=81#next">
                                Mascotas</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=82#next">
                                Panadería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=83#next">
                                Papelería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=84#next">
                                Pescadería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=85#next">
                                Pintura</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=86#next">
                                Sigo Car</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=87#next">
                                Ropa</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=88#next">
                                Sonido y Video</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=2&sub=89#next">
                                Víveres</a>
                                </div>
                                                    </div>
                                            </li>
                	                    <li><a href="contenido.php?cont=3#next">SuperMarket</a>
                                                <div class="dropdown_3columns">
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=43#next">
                                Bodegón</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=19#next">
                                Carnicería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=44#next">
                                Charcutería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=45#next">
                                Chucherías</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=46#next">
                                Congelados</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=22#next">
                                Cuidado Personal</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=21#next">
                                Farmacia</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=18#next">
                                Frulever</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=49#next">
                                Limpieza</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=47#next">
                                Mascotas</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=33#next">
                                Panadería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=42#next">
                                Pescadería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=3&sub=48#next">
                                Víveres</a>
                                </div>
                                                    </div>
                                            </li>
                	                    <li><a href="contenido.php?cont=4#next">HomeMarket</a>
                                                <div class="dropdown_3columns">
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=50#next">
                                Artículos del Hogar</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=51#next">
                                Bebés</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=52#next">
                                Computación</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=53#next">
                                Deportes</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=54#next">
                                Electrodomésticos</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=56#next">
                                Ferretería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=57#next">
                                Lámparas</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=58#next">
                                Lencería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=59#next">
                                Papelería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=60#next">
                                Pintura</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=61#next">
                                Sigo Car</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=62#next">
                                Ropa</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=4&sub=63#next">
                                Sonido y Video</a>
                                </div>
                                                    </div>
                                            </li>
                	                    <li><a href="contenido.php?cont=5#next">MiniMarket</a>
                                                <div class="dropdown_3columns">
                                                            <div class="col_1"><a href="contenido_sub.php?cont=5&sub=91#next">
                                Chucherías</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=5&sub=92#next">
                                Cuidado Personal</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=5&sub=90#next">
                                Farmacia</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=5&sub=93#next">
                                Limpieza</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=5&sub=94#next">
                                Mascotas</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=5&sub=95#next">
                                Panadería</a>
                                </div>
                                                            <div class="col_1"><a href="contenido_sub.php?cont=5&sub=96#next">
                                Víveres</a>
                                </div>
                                                    </div>
                                            </li>
                	                    <li><a href="contenido.php?cont=7#next">Bodegón</a>
                                            </li>
                	                    <li><a href="contenido.php?cont=8#next">Farmacia</a>
                                            </li>
                	            </ul>
        </div>
</div>
<a id="next"></a><br />
<div class="titulo">Sucursales de Sigo en el territorio Venezolano</div>
	<script>
      function initialize() {
        var mapOptions = {
          zoom: 6,
          center: new google.maps.LatLng(6.8828, -66.228882),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('map3'),
                                      mapOptions);

        setMarkers(map, beaches);
      }
      var beaches = [['Universidad Corporativa de Sigo', 10.95143, -63.870564, 1, '1351100806.png'], ['Smilies', 10.952742, -63.868504, 2, '1392392723.png'], ['Farmacia', 10.956971, -63.849603, 3, '1353096580.png'], ['Bodegón ', 10.979393, -63.821032, 4, '1353010652.png'], ['Víveres MiniMarket', 10.99076, -63.8263, 5, '1354052502.png'], ['Panadería y Pastelería MiniMarket', 10.99076, -63.8263, 6, '1353096683.png'], ['Mascotas MiniMarket', 10.99076, -63.8263, 7, '1353096708.png'], ['Limpieza MiniMarket', 10.99076, -63.8263, 8, '1353096736.png'], ['Farmacia Minimarket', 10.953832, -63.868167, 9, '1353096800.png'], ['Cuidado  Personal MiniMarket', 10.99076, -63.8263, 10, '1353096818.png'], ['Chucherías MiniMarket', 10.99076, -63.8263, 11, '1353096836.png'], ['MiniMarket', 11.243062, -63.799255, 12, '1353096904.png'], ['Pintura HomeMarket', 10.99076, -63.8263, 13, '1353096923.png'], ['Sonido y Vídeo ', 10.99076, -63.8263, 14, '1353096945.png'], ['Papelería HomeMarket', 10.99076, -63.8263, 15, '1353096965.png'], ['Víveres SuperMarket', 10.99909, -63.814152, 16, '1353097653.png'], ['Pescadería SuperMarket ', 9.726326, -63.154287, 17, '1353097779.png'], ['Mascotas SuperMarket', 10.99909, -63.814152, 18, '1353097799.png'], ['Limpieza SuperMarket', 10.99909, -63.814152, 19, '1353097817.png'], ['Víveres  HyperMarket', 9.726379, -63.154244, 20, '1353098025.png'], ['Sonido y Video  HyperMarket', 9.726379, -63.154244, 21, '1353098121.png'], ['Pescadería  HyperMarket', 9.726379, -63.154244, 22, '1353098177.png'], ['Papelería HyperMarket', 9.726379, -63.154244, 23, '1353098213.png'], ['Mascotas HyperMarket', 9.726379, -63.154244, 24, '1353098272.png'], ['Limpieza HyperMarket', 9.726379, -63.154244, 25, '1353098309.png'], ['Ropa  HyperMarket', 9.726379, -63.154244, 26, '1353098344.png'], ['Sigo Car  HyperMarket', 9.726379, -63.154244, 27, '1353098386.png'], ['Pintura  HyperMarket', 9.726379, -63.154244, 28, '1353098511.png'], ['Panadería  HyperMarket', 9.726379, -63.154244, 29, '1353098879.png'], ['Lencería HyperMarket', 9.726379, -63.154244, 30, '1353098588.png'], ['Lámparas HyperMarket', 9.726379, -63.154244, 31, '1353098632.png'], ['Frulever HyperMarket', 9.726379, -63.154244, 32, '1353098941.png'], ['Ferretería HyperMarket', 9.726379, -63.134244, 33, '1353098980.png'], ['Electrodomésticos HyperMarket', 9.726379, -63.154244, 34, '1353099024.png'], ['Farmacia HyperMarket', 9.726379, -63.154244, 35, '1353099067.png'], ['Deportes HyperMarket', 9.726379, -63.154244, 36, '1353099111.png'], ['Cuidado Personal HyperMarket', 9.726379, -63.154244, 37, '1353099153.png'], ['Congelados HyperMarket', 9.726379, -63.154244, 38, '1353099189.png'], ['Computación HyperMarket', 9.726379, -63.154244, 39, '1353099252.png'], ['Chucherías HyperMarket', 9.726379, -63.154244, 40, '1353099286.png'], ['Charcutería HyperMarket', 9.726379, -63.154244, 41, '1353099321.png'], ['Carnicería HyperMarket', 9.726379, -63.154244, 42, '1353099356.png'], ['Bodegón HyperMarket', 9.726379, -63.154244, 43, '1353099400.png'], ['Bebés HyperMarket', 9.726379, -63.154244, 44, '1353099433.png'], ['Artículos del Hogar HyperMarket', 9.726379, -63.154244, 45, '1353099469.png'], ['HyperMarket', 9.726379, -63.154244, 46, '1353099513.png'], ['Ropa', 10.99076, -63.8263, 47, '1353099615.png'], ['Sigo Car', 10.99076, -63.8263, 48, '1353099700.png'], ['Lencería', 10.99076, -63.8263, 49, '1353099640.png'], ['Lámparas', 10.99076, -63.8263, 50, '1353099729.png'], ['Ferretería', 10.99076, -63.8263, 51, '1353099785.png'], ['Electrodomésticos', 10.99076, -63.8263, 52, '1353099819.png'], ['Deportes ', 10.99076, -63.8263, 53, '1353100075.png'], ['HomeMarket Computación', 10.99076, -63.8263, 54, '1353099939.png'], ['HomeMarket Bebés', 10.99076, -63.8263, 55, '1353100137.png'], ['HomeMarket Artículos del Hogar', 10.99076, -63.8263, 56, '1353100166.png'], ['HomeMarket', 10.952637, -63.870902, 57, '1353100309.png'], ['Farmacia', 10.99909, -63.814152, 58, '1353182696.png'], ['Cuidado Personal', 10.99909, -63.814152, 59, '1353100752.png'], ['Congelados', 10.99909, -63.814152, 60, '1353182770.png'], ['Chucherías', 10.99909, -63.814152, 61, '1353182815.png'], ['Charcutería', 10.99909, -63.814152, 62, '1353182856.png'], ['Bodegón', 10.99909, -63.814152, 63, '1353182923.png'], ['Carnicería', 10.99909, -63.814152, 64, '1353182978.png'], ['Frulever', 10.99909, -63.814152, 65, '1353183017.png'], ['Panadería Sigo', 10.99909, -63.814152, 66, '1353183080.png'], ['Sigo SuperMarket', 10.99909, -63.814152, 67, '1353183131.png']];

      function setMarkers(map, locations) {var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[0][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[0][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Universidad Corporativa de Sigo" border="0" src="/imagenes/mapa/1351100806.png" class="opacidad fotos2" />'+
			'<b>Universidad Corporativa de Sigo</b></p>'+
            '<p><a href="contenido.php?cont=19">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow0 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[0];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker0 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker0, 'click', function() {
			infowindow0.open(map,marker0);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[1][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[1][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Smilies" border="0" src="/imagenes/mapa/1392392723.png" class="opacidad fotos2" />'+
			'<b>Smilies</b></p>'+
            '<p><a href="contenido.php?cont=9">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow1 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[1];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker1 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker1, 'click', function() {
			infowindow1.open(map,marker1);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[2][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[2][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Farmacia" border="0" src="/imagenes/mapa/1353096580.png" class="opacidad fotos2" />'+
			'<b>Farmacia</b></p>'+
            '<p><a href="contenido.php?cont=8">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow2 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[2];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker2 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker2, 'click', function() {
			infowindow2.open(map,marker2);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[3][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[3][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Bodegón " border="0" src="/imagenes/mapa/1353010652.png" class="opacidad fotos2" />'+
			'<b>Bodegón </b></p>'+
            '<p><a href="contenido.php?cont=7">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow3 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[3];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker3 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker3, 'click', function() {
			infowindow3.open(map,marker3);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[4][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[4][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Víveres MiniMarket" border="0" src="/imagenes/mapa/1354052502.png" class="opacidad fotos2" />'+
			'<b>Víveres MiniMarket</b></p>'+
            '<p><a href="contenido.php?cont=5">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow4 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[4];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker4 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker4, 'click', function() {
			infowindow4.open(map,marker4);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[5][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[5][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Panadería y Pastelería MiniMarket" border="0" src="/imagenes/mapa/1353096683.png" class="opacidad fotos2" />'+
			'<b>Panadería y Pastelería MiniMarket</b></p>'+
            '<p><a href="contenido.php?cont=5">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow5 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[5];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker5 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker5, 'click', function() {
			infowindow5.open(map,marker5);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[6][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[6][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Mascotas MiniMarket" border="0" src="/imagenes/mapa/1353096708.png" class="opacidad fotos2" />'+
			'<b>Mascotas MiniMarket</b></p>'+
            '<p><a href="contenido.php?cont=5">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow6 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[6];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker6 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker6, 'click', function() {
			infowindow6.open(map,marker6);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[7][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[7][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Limpieza MiniMarket" border="0" src="/imagenes/mapa/1353096736.png" class="opacidad fotos2" />'+
			'<b>Limpieza MiniMarket</b></p>'+
            '<p><a href="contenido.php?cont=5">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow7 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[7];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker7 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker7, 'click', function() {
			infowindow7.open(map,marker7);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[8][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[8][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Farmacia Minimarket" border="0" src="/imagenes/mapa/1353096800.png" class="opacidad fotos2" />'+
			'<b>Farmacia Minimarket</b></p>'+
            '<p><a href="contenido.php?cont=5">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow8 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[8];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker8 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker8, 'click', function() {
			infowindow8.open(map,marker8);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[9][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[9][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Cuidado  Personal MiniMarket" border="0" src="/imagenes/mapa/1353096818.png" class="opacidad fotos2" />'+
			'<b>Cuidado  Personal MiniMarket</b></p>'+
            '<p><a href="contenido.php?cont=5">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow9 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[9];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker9 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker9, 'click', function() {
			infowindow9.open(map,marker9);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[10][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[10][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Chucherías MiniMarket" border="0" src="/imagenes/mapa/1353096836.png" class="opacidad fotos2" />'+
			'<b>Chucherías MiniMarket</b></p>'+
            '<p><a href="contenido.php?cont=5">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow10 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[10];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker10 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker10, 'click', function() {
			infowindow10.open(map,marker10);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[11][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[11][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="MiniMarket" border="0" src="/imagenes/mapa/1353096904.png" class="opacidad fotos2" />'+
			'<b>MiniMarket</b></p>'+
            '<p><a href="contenido.php?cont=5">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow11 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[11];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker11 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker11, 'click', function() {
			infowindow11.open(map,marker11);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[12][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[12][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Pintura HomeMarket" border="0" src="/imagenes/mapa/1353096923.png" class="opacidad fotos2" />'+
			'<b>Pintura HomeMarket</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow12 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[12];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker12 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker12, 'click', function() {
			infowindow12.open(map,marker12);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[13][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[13][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Sonido y Vídeo " border="0" src="/imagenes/mapa/1353096945.png" class="opacidad fotos2" />'+
			'<b>Sonido y Vídeo </b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow13 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[13];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker13 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker13, 'click', function() {
			infowindow13.open(map,marker13);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[14][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[14][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Papelería HomeMarket" border="0" src="/imagenes/mapa/1353096965.png" class="opacidad fotos2" />'+
			'<b>Papelería HomeMarket</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow14 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[14];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker14 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker14, 'click', function() {
			infowindow14.open(map,marker14);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[15][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[15][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Víveres SuperMarket" border="0" src="/imagenes/mapa/1353097653.png" class="opacidad fotos2" />'+
			'<b>Víveres SuperMarket</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow15 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[15];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker15 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker15, 'click', function() {
			infowindow15.open(map,marker15);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[16][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[16][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Pescadería SuperMarket " border="0" src="/imagenes/mapa/1353097779.png" class="opacidad fotos2" />'+
			'<b>Pescadería SuperMarket </b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow16 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[16];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker16 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker16, 'click', function() {
			infowindow16.open(map,marker16);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[17][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[17][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Mascotas SuperMarket" border="0" src="/imagenes/mapa/1353097799.png" class="opacidad fotos2" />'+
			'<b>Mascotas SuperMarket</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow17 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[17];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker17 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker17, 'click', function() {
			infowindow17.open(map,marker17);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[18][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[18][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Limpieza SuperMarket" border="0" src="/imagenes/mapa/1353097817.png" class="opacidad fotos2" />'+
			'<b>Limpieza SuperMarket</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow18 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[18];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker18 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker18, 'click', function() {
			infowindow18.open(map,marker18);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[19][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[19][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Víveres  HyperMarket" border="0" src="/imagenes/mapa/1353098025.png" class="opacidad fotos2" />'+
			'<b>Víveres  HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow19 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[19];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker19 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker19, 'click', function() {
			infowindow19.open(map,marker19);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[20][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[20][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Sonido y Video  HyperMarket" border="0" src="/imagenes/mapa/1353098121.png" class="opacidad fotos2" />'+
			'<b>Sonido y Video  HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow20 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[20];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker20 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker20, 'click', function() {
			infowindow20.open(map,marker20);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[21][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[21][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Pescadería  HyperMarket" border="0" src="/imagenes/mapa/1353098177.png" class="opacidad fotos2" />'+
			'<b>Pescadería  HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow21 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[21];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker21 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker21, 'click', function() {
			infowindow21.open(map,marker21);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[22][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[22][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Papelería HyperMarket" border="0" src="/imagenes/mapa/1353098213.png" class="opacidad fotos2" />'+
			'<b>Papelería HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow22 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[22];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker22 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker22, 'click', function() {
			infowindow22.open(map,marker22);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[23][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[23][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Mascotas HyperMarket" border="0" src="/imagenes/mapa/1353098272.png" class="opacidad fotos2" />'+
			'<b>Mascotas HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow23 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[23];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker23 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker23, 'click', function() {
			infowindow23.open(map,marker23);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[24][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[24][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Limpieza HyperMarket" border="0" src="/imagenes/mapa/1353098309.png" class="opacidad fotos2" />'+
			'<b>Limpieza HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow24 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[24];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker24 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker24, 'click', function() {
			infowindow24.open(map,marker24);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[25][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[25][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Ropa  HyperMarket" border="0" src="/imagenes/mapa/1353098344.png" class="opacidad fotos2" />'+
			'<b>Ropa  HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow25 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[25];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker25 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker25, 'click', function() {
			infowindow25.open(map,marker25);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[26][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[26][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Sigo Car  HyperMarket" border="0" src="/imagenes/mapa/1353098386.png" class="opacidad fotos2" />'+
			'<b>Sigo Car  HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow26 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[26];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker26 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker26, 'click', function() {
			infowindow26.open(map,marker26);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[27][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[27][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Pintura  HyperMarket" border="0" src="/imagenes/mapa/1353098511.png" class="opacidad fotos2" />'+
			'<b>Pintura  HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow27 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[27];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker27 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker27, 'click', function() {
			infowindow27.open(map,marker27);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[28][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[28][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Panadería  HyperMarket" border="0" src="/imagenes/mapa/1353098879.png" class="opacidad fotos2" />'+
			'<b>Panadería  HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow28 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[28];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker28 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker28, 'click', function() {
			infowindow28.open(map,marker28);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[29][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[29][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Lencería HyperMarket" border="0" src="/imagenes/mapa/1353098588.png" class="opacidad fotos2" />'+
			'<b>Lencería HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow29 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[29];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker29 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker29, 'click', function() {
			infowindow29.open(map,marker29);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[30][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[30][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Lámparas HyperMarket" border="0" src="/imagenes/mapa/1353098632.png" class="opacidad fotos2" />'+
			'<b>Lámparas HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow30 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[30];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker30 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker30, 'click', function() {
			infowindow30.open(map,marker30);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[31][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[31][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Frulever HyperMarket" border="0" src="/imagenes/mapa/1353098941.png" class="opacidad fotos2" />'+
			'<b>Frulever HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow31 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[31];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker31 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker31, 'click', function() {
			infowindow31.open(map,marker31);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[32][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[32][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Ferretería HyperMarket" border="0" src="/imagenes/mapa/1353098980.png" class="opacidad fotos2" />'+
			'<b>Ferretería HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow32 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[32];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker32 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker32, 'click', function() {
			infowindow32.open(map,marker32);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[33][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[33][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Electrodomésticos HyperMarket" border="0" src="/imagenes/mapa/1353099024.png" class="opacidad fotos2" />'+
			'<b>Electrodomésticos HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow33 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[33];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker33 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker33, 'click', function() {
			infowindow33.open(map,marker33);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[34][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[34][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Farmacia HyperMarket" border="0" src="/imagenes/mapa/1353099067.png" class="opacidad fotos2" />'+
			'<b>Farmacia HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow34 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[34];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker34 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker34, 'click', function() {
			infowindow34.open(map,marker34);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[35][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[35][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Deportes HyperMarket" border="0" src="/imagenes/mapa/1353099111.png" class="opacidad fotos2" />'+
			'<b>Deportes HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow35 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[35];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker35 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker35, 'click', function() {
			infowindow35.open(map,marker35);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[36][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[36][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Cuidado Personal HyperMarket" border="0" src="/imagenes/mapa/1353099153.png" class="opacidad fotos2" />'+
			'<b>Cuidado Personal HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow36 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[36];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker36 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker36, 'click', function() {
			infowindow36.open(map,marker36);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[37][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[37][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Congelados HyperMarket" border="0" src="/imagenes/mapa/1353099189.png" class="opacidad fotos2" />'+
			'<b>Congelados HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow37 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[37];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker37 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker37, 'click', function() {
			infowindow37.open(map,marker37);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[38][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[38][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Computación HyperMarket" border="0" src="/imagenes/mapa/1353099252.png" class="opacidad fotos2" />'+
			'<b>Computación HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow38 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[38];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker38 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker38, 'click', function() {
			infowindow38.open(map,marker38);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[39][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[39][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Chucherías HyperMarket" border="0" src="/imagenes/mapa/1353099286.png" class="opacidad fotos2" />'+
			'<b>Chucherías HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow39 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[39];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker39 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker39, 'click', function() {
			infowindow39.open(map,marker39);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[40][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[40][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Charcutería HyperMarket" border="0" src="/imagenes/mapa/1353099321.png" class="opacidad fotos2" />'+
			'<b>Charcutería HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow40 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[40];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker40 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker40, 'click', function() {
			infowindow40.open(map,marker40);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[41][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[41][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Carnicería HyperMarket" border="0" src="/imagenes/mapa/1353099356.png" class="opacidad fotos2" />'+
			'<b>Carnicería HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow41 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[41];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker41 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker41, 'click', function() {
			infowindow41.open(map,marker41);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[42][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[42][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Bodegón HyperMarket" border="0" src="/imagenes/mapa/1353099400.png" class="opacidad fotos2" />'+
			'<b>Bodegón HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow42 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[42];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker42 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker42, 'click', function() {
			infowindow42.open(map,marker42);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[43][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[43][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Bebés HyperMarket" border="0" src="/imagenes/mapa/1353099433.png" class="opacidad fotos2" />'+
			'<b>Bebés HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow43 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[43];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker43 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker43, 'click', function() {
			infowindow43.open(map,marker43);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[44][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[44][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Artículos del Hogar HyperMarket" border="0" src="/imagenes/mapa/1353099469.png" class="opacidad fotos2" />'+
			'<b>Artículos del Hogar HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow44 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[44];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker44 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker44, 'click', function() {
			infowindow44.open(map,marker44);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[45][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[45][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="HyperMarket" border="0" src="/imagenes/mapa/1353099513.png" class="opacidad fotos2" />'+
			'<b>HyperMarket</b></p>'+
            '<p><a href="contenido.php?cont=2">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow45 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[45];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker45 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker45, 'click', function() {
			infowindow45.open(map,marker45);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[46][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[46][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Ropa" border="0" src="/imagenes/mapa/1353099615.png" class="opacidad fotos2" />'+
			'<b>Ropa</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow46 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[46];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker46 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker46, 'click', function() {
			infowindow46.open(map,marker46);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[47][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[47][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Sigo Car" border="0" src="/imagenes/mapa/1353099700.png" class="opacidad fotos2" />'+
			'<b>Sigo Car</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow47 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[47];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker47 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker47, 'click', function() {
			infowindow47.open(map,marker47);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[48][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[48][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Lencería" border="0" src="/imagenes/mapa/1353099640.png" class="opacidad fotos2" />'+
			'<b>Lencería</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow48 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[48];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker48 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker48, 'click', function() {
			infowindow48.open(map,marker48);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[49][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[49][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Lámparas" border="0" src="/imagenes/mapa/1353099729.png" class="opacidad fotos2" />'+
			'<b>Lámparas</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow49 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[49];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker49 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker49, 'click', function() {
			infowindow49.open(map,marker49);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[50][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[50][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Ferretería" border="0" src="/imagenes/mapa/1353099785.png" class="opacidad fotos2" />'+
			'<b>Ferretería</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow50 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[50];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker50 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker50, 'click', function() {
			infowindow50.open(map,marker50);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[51][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[51][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Electrodomésticos" border="0" src="/imagenes/mapa/1353099819.png" class="opacidad fotos2" />'+
			'<b>Electrodomésticos</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow51 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[51];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker51 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker51, 'click', function() {
			infowindow51.open(map,marker51);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[52][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[52][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Deportes " border="0" src="/imagenes/mapa/1353100075.png" class="opacidad fotos2" />'+
			'<b>Deportes </b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow52 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[52];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker52 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker52, 'click', function() {
			infowindow52.open(map,marker52);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[53][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[53][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="HomeMarket Computación" border="0" src="/imagenes/mapa/1353099939.png" class="opacidad fotos2" />'+
			'<b>HomeMarket Computación</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow53 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[53];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker53 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker53, 'click', function() {
			infowindow53.open(map,marker53);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[54][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[54][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="HomeMarket Bebés" border="0" src="/imagenes/mapa/1353100137.png" class="opacidad fotos2" />'+
			'<b>HomeMarket Bebés</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow54 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[54];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker54 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker54, 'click', function() {
			infowindow54.open(map,marker54);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[55][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[55][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="HomeMarket Artículos del Hogar" border="0" src="/imagenes/mapa/1353100166.png" class="opacidad fotos2" />'+
			'<b>HomeMarket Artículos del Hogar</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow55 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[55];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker55 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker55, 'click', function() {
			infowindow55.open(map,marker55);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[56][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[56][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="HomeMarket" border="0" src="/imagenes/mapa/1353100309.png" class="opacidad fotos2" />'+
			'<b>HomeMarket</b></p>'+
            '<p><a href="contenido.php?cont=4">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow56 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[56];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker56 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker56, 'click', function() {
			infowindow56.open(map,marker56);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[57][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[57][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Farmacia" border="0" src="/imagenes/mapa/1353182696.png" class="opacidad fotos2" />'+
			'<b>Farmacia</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow57 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[57];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker57 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker57, 'click', function() {
			infowindow57.open(map,marker57);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[58][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[58][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Cuidado Personal" border="0" src="/imagenes/mapa/1353100752.png" class="opacidad fotos2" />'+
			'<b>Cuidado Personal</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow58 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[58];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker58 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker58, 'click', function() {
			infowindow58.open(map,marker58);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[59][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[59][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Congelados" border="0" src="/imagenes/mapa/1353182770.png" class="opacidad fotos2" />'+
			'<b>Congelados</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow59 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[59];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker59 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker59, 'click', function() {
			infowindow59.open(map,marker59);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[60][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[60][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Chucherías" border="0" src="/imagenes/mapa/1353182815.png" class="opacidad fotos2" />'+
			'<b>Chucherías</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow60 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[60];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker60 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker60, 'click', function() {
			infowindow60.open(map,marker60);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[61][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[61][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Charcutería" border="0" src="/imagenes/mapa/1353182856.png" class="opacidad fotos2" />'+
			'<b>Charcutería</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow61 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[61];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker61 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker61, 'click', function() {
			infowindow61.open(map,marker61);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[62][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[62][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Bodegón" border="0" src="/imagenes/mapa/1353182923.png" class="opacidad fotos2" />'+
			'<b>Bodegón</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow62 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[62];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker62 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker62, 'click', function() {
			infowindow62.open(map,marker62);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[63][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[63][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Carnicería" border="0" src="/imagenes/mapa/1353182978.png" class="opacidad fotos2" />'+
			'<b>Carnicería</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow63 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[63];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker63 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker63, 'click', function() {
			infowindow63.open(map,marker63);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[64][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[64][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Frulever" border="0" src="/imagenes/mapa/1353183017.png" class="opacidad fotos2" />'+
			'<b>Frulever</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow64 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[64];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker64 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker64, 'click', function() {
			infowindow64.open(map,marker64);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[65][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[65][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Panadería Sigo" border="0" src="/imagenes/mapa/1353183080.png" class="opacidad fotos2" />'+
			'<b>Panadería Sigo</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow65 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[65];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker65 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker65, 'click', function() {
			infowindow65.open(map,marker65);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[66][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[66][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="Sigo SuperMarket" border="0" src="/imagenes/mapa/1353183131.png" class="opacidad fotos2" />'+
			'<b>Sigo SuperMarket</b></p>'+
            '<p><a href="contenido.php?cont=3">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow66 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[66];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker66 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker66, 'click', function() {
			infowindow66.open(map,marker66);});var image = new google.maps.MarkerImage('/imagenes/mapa/'+locations[67][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/mapa/'+locations[67][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><img width="140" alt="" border="0" src="/imagenes/mapa/" class="opacidad fotos2" />'+
			'<b></b></p>'+
            '<p><a href="contenido.php?cont=">'+
            'Ver m�s ++</a></p>'+
            '</div>'+
            '</div>';
	
			var infowindow67 = new google.maps.InfoWindow({
				content: contentString
			});
		
			var beach = locations[67];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker67 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
			  
			google.maps.event.addListener(marker67, 'click', function() {
			infowindow67.open(map,marker67);});} google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  <div id="map3"></div>
  
  <div id="menu_categorias">
    <div id="redes2">
   		<div class="redes_icono2"> <a href="http://twitter.com/sigosa"><img src="/imagenes/logo_twitter_gris.png" width="46" height="46" border="0" id="Image1" onmouseover="MM_swapImage('Image1','','/imagenes/logo_twitter_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.facebook.com/SigoVenezuela"><img src="/imagenes/logo_face_gris.png" width="46" height="46" border="0" id="Image2" onmouseover="MM_swapImage('Image2','','/imagenes/logo_facebook_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.youtube.com/SigoVenezuela"><img src="/imagenes/logo_youtube_gris.png" width="46" height="46" border="0" id="Image3" onmouseover="MM_swapImage('Image3','','/imagenes/logo_youtube_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
</div>
	
    		<div class="modulo">
            <div><a href="#" title="Sigo">
            <span class="titulo4">Sigo</span></a></div>
                                            <div class="submodulos">
                <a href="contenido_sub.php?cont=15&sub=97#next" title="La Empresa Sigo S.A." class="enlace_top">&bull; ¿Quiénes Somos?</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=15&sub=98#next" title="Misión y Visión Sigo S.A." class="enlace_top">&bull; Misión y Visión</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=15&sub=99#next" title="Sala de Prensa" class="enlace_top">&bull; Sala de Prensa</a></div>
                                    </div>
     		<div class="modulo">
            <div><a href="#" title="Sucursales">
            <span class="titulo4">Sucursales</span></a></div>
                                            <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=106#next" title="Sucursales de Porlamar" class="enlace_top">&bull; Porlamar</a></div>
                                <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=105#next" title="Sucursales de Pampatar" class="enlace_top">&bull; Pampatar</a></div>
                                <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=100#next" title="Sucursal de Barcelona" class="enlace_top">&bull; Barcelona</a></div>
                                <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=103#next" title="Sucursales de Maturín" class="enlace_top">&bull; Maturín</a></div>
                                <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=104#next" title="Sucursales de Paraguaná" class="enlace_top">&bull; Paraguaná</a></div>
                                    </div>
     		<div class="modulo">
            <div><a href="#" title="Corporativo">
            <span class="titulo4">Corporativo</span></a></div>
                                            <div class="submodulos">
                <a href="contenido_sub.php?cont=17&sub=109#next" title="Innova Centros Comerciales" class="enlace_top">&bull; Innova Centros Comerciales</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=17&sub=110#next" title="Intranet Sigo S.A." class="enlace_top">&bull; Intranet</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=17&sub=115#next" title="Empleo Sigo" class="enlace_top">&bull; Empleo</a></div>
                                    </div>
     		<div class="modulo">
            <div><a href="#" title="Pensando en Tí">
            <span class="titulo4">Pensando en Tí</span></a></div>
                                            <div class="submodulos">
                <a href="contenido_sub.php?cont=18&sub=111#next" title="Valoramos tu bienestar" class="enlace_top">&bull; Valoramos tu bienestar</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=18&sub=112#next" title="Eventos Sigo S.A." class="enlace_top">&bull; Eventos</a></div>
                                    </div>
     	
</div>
</div>
<div class="clear"></div>

<div id="panel_pie">
    <div id="pie">
    <div id="botonera">
    	<div class="boton"><a href="index.php">Home</a></div>
                    <div class="boton"><a href="contenido.php?cont=9#next">Smilies</a></div>
                    <div class="boton"><a href="contenido.php?cont=20#next">Responsabilidad Social</a></div>
                    <div class="boton"><a href="contenido.php?cont=19#next">Universidad Corporativa de Sigo</a></div>
                <div class="boton"><a href="#">Cont&aacute;ctanos</a></div>
        <div class="boton_less">&nbsp;</div>
    </div>
    <div id="logominimo"><img src="/imagenes/loguito.png" width="50" height="45" /></div>
    <div id="copy"> | SIGO S.A. ® 2016 RIF:J-08003048-6</div>
  </div>
</div>
</body>
</html>