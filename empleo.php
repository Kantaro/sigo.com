<?php
/* header para Smarty */
require('config/setup.php');
$smarty = new objeto_smarty;
/*  Fin header para Smarty */

include_once ("config/class.login.php");
include_once ("config/class.link.php");
include_once ("config/class.contenido.php");
include_once ("config/class.categoria.php");
include_once ("config/class.publicidad.php");
include_once ("config/class.mapa.php");
include_once ("config/class.banner.php");
include_once ("config/class.galeria.php");
include_once("config/conexion.inc.php");
require('smarty/libs/SmartyPaginate.class.php');

session_start();
if(!isset($acceso))
	$acceso = new Auth;
if ($_POST){
	if ($_POST['enviar'] == "Login"){
		$acceso->asignar_consulta($_POST['login'],$_POST['clave']);
		$acceso->login2($acceso->login, $acceso->password);
	};
}
if ($_GET){
	if ($_GET['enviar'] == "Logout")
		$acceso->logout();
}

SmartyPaginate::disconnect();
// required connect
SmartyPaginate::connect();
// set items per page
SmartyPaginate::setLimit(10);
SmartyPaginate::setPrevText('Anterior');
SmartyPaginate::setNextText('Siguiente');
SmartyPaginate::setFirstText('Primer');
SmartyPaginate::setLastText('�ltimo');

if(isset($_GET['cont']) && $_GET['cont']!="")
	$cont=$_GET['cont'];

if($cont==1){
	header("location: index.html");
	exit();
}

if(!isset($galeria))
	$galeria= new Galeria;
$galeria->listar_galeria_publica2();

if(!isset($galeria2))
	$galeria2= new Galeria;
$galeria2->listar_galeria_publica3();

if(!isset($banner))
	$banner= new Banner;
$banner->listar_banner_publica($cont);

if(!isset($link))
	$link= new Link;
$link->listar_link_menu("todo");
$link->mostrar_link_publico($cont);

if(!isset($enlaces_A))
	$enlaces_A= new Link();
$enlaces_A->listar_link_menu("arriba");

if(!isset($enlaces_B))
	$enlaces_B= new Link();
$enlaces_B->listar_link_menu("central");

if(!isset($enlaces_C))
	$enlaces_C= new Link();
$enlaces_C->listar_link_menu("abajo");

if(!isset($contenido))
	$contenido= new Contenido();
$contenido->listar_contenido_imagen($cont);
$data=$contenido->listado;

if($contenido->mensaje!="si"){
	$mensaje2="<tr><td align='center' colspan='2' class='error'>No existen registros en esta secci�n</td></tr>";	
}

if(!isset($sublink))
	$sublink= new Link();
$sublink->cargar_sublink();

function get_db_results($_data) {
	SmartyPaginate::setTotal(count($_data));
	return array_slice($_data, SmartyPaginate::getCurrentIndex(),
		SmartyPaginate::getLimit());
}

SmartyPaginate::setUrl("contenido.php?cont=".$_GET['cont']);

if(!isset($publicidad))
	$publicidad= new Publicidad;
$publicidad->cargar_publicidad("Banner Izquierdo");

$smarty->assign("publicidad", $publicidad->listado);

if(!isset($publicidad2))
	$publicidad2= new Publicidad;
$publicidad2->cargar_publicidad("Banner Derecho");

$smarty->assign("publicidad2", $publicidad2->listado);
	
/* footer para Smarty */
$smarty->assign("logo", $acceso->logo);
$smarty->assign("mapas", $mapas);
$smarty->assign("categoria", $nombre);
$smarty->assign("mensaje2", $mensaje2);
$smarty->assign("cont", $cont);
$smarty->assign("enlaces", $link->listado);
$smarty->assign('enlaces_A',$enlaces_A->listado);
$smarty->assign('enlaces_B',$enlaces_B->listado);
$smarty->assign('enlaces_C',$enlaces_C->listado);
$smarty->assign("banner", $banner->listado);
$smarty->assign("revistas", $galeria->listado);
$smarty->assign("revistas2", $galeria2->listado);

$smarty->assign("enlaces", $link->listado);
$smarty->assign('subcategorias',$sublink->listado);
if(isset($data) && $data!=""){
	$smarty->assign('contenido', get_db_results($data));
	// assign {$paginate} var
	SmartyPaginate::assign($smarty);
	// display results
}

$smarty->assign("accion", "Empleo | Trabaja con Nosotros");
$smarty->assign("descripcion", $link->descripcion);
$smarty->assign("claves", $link->claves);

$smarty->force_compile=true;
$smarty->display('empleo.tpl');
/* Fin footer para Smarty */
?>