<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_padre.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<title>{$accion} | Sigo Venezuela</title>
<meta name="Title" content="Sigo S.A." />
<meta name="Author" content="Alejandro D&iacute;az http://diazcreativos.net.ve/" />
<meta name="Subject" content="Sigo S.A." />
<meta name="Description" content="{$descripcion}" /> 
<meta name="Keywords" content="{$claves}" />

<meta name="Generator" content="DreamWeaver" /> 
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="7 days" /> 
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />

<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico" />
<script src="/js/validar.js" type="text/javascript"></script>
<link href="/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />
<link rel="stylesheet" href="/css/jshowoff.css" type="text/css" media="screen, projection" />
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery.skitter.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jshowoff.min.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.js"></script>
<link rel="stylesheet" type="text/css" href="/css/skin.css" />
<link href="/css/styles.css" type="text/css" media="all" rel="stylesheet" />
<link href="/dropdown/menu.css" type="text/css" media="all" rel="stylesheet" />
{literal}
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
		vertical: true,
        auto: 4,
		easing: 'easeOutBack',
		animation: 'slow',
		scroll: 3,
        wrap: 'circular',
        initCallback: mycarousel_initCallback,
		itemLoadCallback: itemLoadCallbackFunction
    });
});

$(function(){
    // Skitter
    $('.box_skitter_large').skitter();
    
});
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6623041-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

{/literal}
<!-- InstanceBeginEditable name="head" -->
{literal}
<script type="text/javascript" src="/js/jquery.lightbox-0.5.js"></script>
<link rel="stylesheet" type="text/css" href="/css/jquery.lightbox-0.5.css" media="screen" />
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
});

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
  {lang: 'es'}
</script>

{/literal}
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('/imagenes/logo_twitter_color.png','/imagenes/logo_facebook_color.png','/imagenes/logo_youtube_color.png')">
<div id="main_principal">
<div id="encabezado">
        <div id="logopequeno"><a href="index.php" title="Home"><img src="/imagenes/logotipo_{$logo}.png" width="154" height="144" border="0" /></a></div>
        <div id="botonera_A">
            <ul id="menu">
                    {section name=i loop=$enlaces_A}
                    <li><a href="contenido.php?cont={$enlaces_A[i].id_cat}#next">{$enlaces_A[i].nombre_cat}</a>
                        {if $enlaces_A[i].enlaces neq ""}
                        <div class="dropdown_3columns">
                            {section name=j loop=$enlaces_A[i].enlaces}
                                <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont={$enlaces_A[i].id_cat}&sub={$enlaces_A[i].enlaces[j].id_sub}#next">
                                {$enlaces_A[i].enlaces[j].nombre_sub}</a> -->
                                {$enlaces_A[i].enlaces[j].nombre_sub}
                                </div>
                            {/section}
                        </div>
                        {/if}
                    </li>
                	{/section}
            </ul>
        </div>
        
        <div id="topbanner">
        	<div class="box_skitter box_skitter_large">  
			<ul>
            	{if $banner neq ""}
                	{section name=i loop=$banner}
					<li>
						<a {if $banner[i].vinculo_ban eq ""} href="#" {else} href="{$banner[i].vinculo_ban}"{/if}>
                        <img alt="{$banner[i].etiqueta_ban}" src="/imagenes/banner/{$banner[i].url_ban}" class="{$banner[i].efecto_ban}" /></a>
<div class="label_text">
							<p>{$banner[i].etiqueta_ban}</p>
					  </div>
					</li>
                    {/section}
                {else}
                	<li>
                        <a href="#" title="Sigo Venezuela">
                        <img alt="Sigo Venezuela" src="/imagenes/banner_principal.png" /></a>
    <div class="label_text">
                            <p class="etiqueta">Sigo Venezuela</p>
                        </div>
                    </li>
                {/if}
			  </ul>
        	</div>
        </div>
    </div>
  <div id="botonera_B">
   	<div id="enlaces">
    	{section name=i loop=$enlaces_B}
    		<div class="boton_B"><a href="contenido.php?cont={$enlaces_B[i].id_cat}#next">{$enlaces_B[i].nombre_cat}</a></div>
        {/section}
    </div>
<div id="redes">
        	<div class="redes_icono">
            <a href="http://www.facebook.com/SigoVenezuela" target="_blank" title="Siguenos es Facebook"><img src="/imagenes/icono_face.png" width="48" height="48" border="0" class="opacidad" /></a></div>
            <div class="redes_icono">
            <a href="http://twitter.com/sigosa" target="_blank" title="Siguenos es Twitter"><img src="/imagenes/icono_twitter.png" width="48" height="48" border="0" class="opacidad" /></a></div>
            <div class="redes_icono">
        <a href="http://www.youtube.com/SigoVenezuela" target="_blank" title="Siguenos es Youtube"><img src="/imagenes/icono_youtube.png" width="48" height="48" border="0" class="opacidad" /></a></div>
        </div> 
  </div>
  <a id="next"></a>
  <!-- InstanceBeginEditable name="contenido" -->
          <div id="entradas">
          	<table width="100%" border="0" align="left" cellpadding="20" cellspacing="0">
            	{if $listado[0].id_image neq ""}
            	<tr>
                	<td><img border="0" src="/admin/galeria/publica_imagen2.php?id={$listado[0].id_image}&amp" class="fotos" alt="{$listado[i].nombre_image}" /></td>
                </tr>
                {/if}
                <tr>
                  <td>
                  <div class="titulo">{$nombre}<span class="titulo_promo"> / {$fecha}</span></div> 
                  {$contenido}</td>
            </tr>
                  <tr>
                    <td align="right"><a href="javascript: history.back();"><img src="/imagenes/atras.png" width="25" height="25" border="0" align="absmiddle" alt="volver a {$enlace}" /> Volver a {$enlace}</a></td>
                  </tr>
              </table>
</div>
          
  <!-- InstanceEndEditable -->
  <div id="menu_categorias">
  <div id="redes2">
   		<div class="redes_icono2"><a href="http://twitter.com/sigosa"><img src="/imagenes/logo_twitter_gris.png" width="46" height="46" border="0" id="Image1" onmouseover="MM_swapImage('Image1','','/imagenes/logo_twitter_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.facebook.com/SigoVenezuela"><img src="/imagenes/logo_face_gris.png" width="46" height="46" border="0" id="Image2" onmouseover="MM_swapImage('Image2','','/imagenes/logo_facebook_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.youtube.com/SigoVenezuela"><img src="/imagenes/logo_youtube_gris.png" width="46" height="46" border="0" id="Image3" onmouseover="MM_swapImage('Image3','','/imagenes/logo_youtube_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
</div>
	
    {section name=i loop=$enlaces_C}
		<div class="modulo">
            <div><a href="#" title="{$enlaces_C[i].nombre_cat}">
            <span class="titulo4">{$enlaces_C[i].nombre_cat}</span></a></div>
            {if $enlaces_C[i].enlaces neq ""}
                {section name=j loop=$enlaces_C[i].enlaces}
                <div {if $enlaces_C[i].id_cat eq "16"}class="submodulos2"{else}class="submodulos"{/if}>
                <a href="contenido_sub.php?cont={$enlaces_C[i].id_cat}&sub={$enlaces_C[i].enlaces[j].id_sub}#next" title="{$enlaces_C[i].enlaces[j].etiqueta_sub}" class="enlace_top">&bull; {$enlaces_C[i].enlaces[j].nombre_sub}</a></div>
                {/section}
            {/if}
        </div>
     {/section}
	
  </div>
</div>
<div class="clear"></div>

<div id="panel_pie">
    <div id="pie">
    <div id="botonera">
    	<div class="boton"><a href="index.php">Home</a></div>
        {section name=i loop=$enlaces_B}
            <div class="boton"><a href="contenido.php?cont={$enlaces_B[i].id_cat}#next">{$enlaces_B[i].nombre_cat}</a></div>
        {/section}
        <div class="boton"><a title="Contactos"  href="contacto.php">Cont&aacute;ctanos</a></div>
        <div class="boton_less">&nbsp;</div>
    </div>
    <div id="logominimo"><img src="/imagenes/loguito.png" width="50" height="45" /></div>
    <div id="copy"> | SIGO S.A. &reg; 2016 RIF:J-08003048-6</div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>
