<?php
/* header para Smarty */
require('config/setup.php');
$smarty = new objeto_smarty;
/*  Fin header para Smarty */

include_once ("config/class.login.php");
include_once ("config/class.link.php");
include_once ("config/class.publicidad.php");
include_once ("config/class.evento.php");
include_once ("config/class.noticia.php");
include_once("config/conexion.inc.php");
include_once ("config/class.categoria.php");
include_once ("config/class.banner.php");
include_once("config/pagination.class.php");

session_start();
if(!isset($acceso))
	$acceso = new Auth;
if ($_POST){
	if ($_POST['enviar'] == "Login"){
		$acceso->asignar_consulta($_POST['login'],$_POST['clave']);
		$acceso->login2($acceso->login, $acceso->password);
	};
}
if ($_GET){
	if ($_GET['enviar'] == "Logout")
		$acceso->logout();
}

if(isset($_POST['valor']) && $_POST['valor']!=""){
	$_SESSION['valor']=$_POST['valor'];
}else if(isset($_GET['valor']) && $_GET['valor']!=""){
	$_SESSION['valor']=$_GET['valor'];
}else if(!isset($_SESSION['valor']) && $_SESSION['valor']==""){
	$_SESSION['valor']="Proximo";
}

if(!isset($evento))
	$evento= new Evento();

if(isset($_GET['fecha']) && $_GET['fecha']!=""){
	$fecha=$_GET['fecha'];
	$evento->buscar_evento($fecha);
}else{
	$evento->listar_evento2($_SESSION['valor']);
}

if(!isset($list_eveto))
	$list_eveto= new Evento;
$list_eveto->listar_evento_publica();

if(!isset($banner))
	$banner= new Banner;
$banner->listar_banner_publica($cont);

if(!isset($link))
	$link= new Link;
$link->listar_link_menu("todo");
$link->mostrar_link_publico($cont);

if(!isset($enlaces_A))
	$enlaces_A= new Link();
$enlaces_A->listar_link_menu("arriba");

if(!isset($enlaces_B))
	$enlaces_B= new Link();
$enlaces_B->listar_link_menu("central");

if(!isset($enlaces_C))
	$enlaces_C= new Link();
$enlaces_C->listar_link_menu("abajo");

if(!isset($sublink))
	$sublink= new Link();
$sublink->cargar_sublink();

if(!isset($publicidad))
	$publicidad= new Publicidad;
$publicidad->cargar_publicidad("Banner Izquierdo");

$smarty->assign("publicidad", $publicidad->listado);

if(!isset($publicidad2))
	$publicidad2= new Publicidad;
$publicidad2->cargar_publicidad("Banner Derecho");

$smarty->assign("publicidad2", $publicidad2->listado);


// Paginación de Registros

$pagination = new pagination();
$dataPages = $pagination->generate($evento->listado, 10);
$smarty->assign('eventos', $dataPages);
//$pagination->showFirstAndLast=true;
//$smarty->assign('pagination', $pagination->links());
$smarty->assign('pagination', $pagination->links());

//------------------------

if(!isset($fechas))
	$fechas= new Evento;
$fechas_validas=$fechas->buscar_fechas();
$smarty->assign("fechas", $fechas_validas);
$smarty->assign("dias", $fechas->dias);

mysql_close($conex);

/* footer para Smarty */
$smarty->assign("logo", $acceso->logo);
$smarty->assign("mapas", $mapas);
$smarty->assign("mensaje2", $mensaje2);
$smarty->assign("cont", $cont);
$smarty->assign("categoria", $nombre);
$smarty->assign("accion", "Eventos y Actividades");

$smarty->assign("enlaces", $link->listado);
$smarty->assign('enlaces_A',$enlaces_A->listado);
$smarty->assign('enlaces_B',$enlaces_B->listado);
$smarty->assign('enlaces_C',$enlaces_C->listado);
$smarty->assign("banner", $banner->listado);
$smarty->assign("listado_eventos", $list_eveto->listado);
$smarty->assign("valor", $_SESSION['valor']);

$smarty->force_compile=true;
$smarty->display('eventos.tpl');
/* Fin footer para Smarty */
?>