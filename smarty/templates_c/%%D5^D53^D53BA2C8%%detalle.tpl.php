<?php /* Smarty version 2.6.9, created on 2016-11-09 11:38:29
         compiled from admin/registro/detalle.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Sigo S.A. - Panel Administrativo</title>
<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico"> 
<script type="text/javascript" language="javascript" src="/js/validar.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<!-- InstanceBeginEditable name="head" -->
<?php echo '
<link rel="stylesheet" type="text/css" media="all" href="/calendario/calendar-blue.css" title="blue" />
<script type="text/javascript" src="/calendario/calendar.js"></script>
<script type="text/javascript" src="/calendario/lang/calendar-en.js"></script>
<script type="text/javascript" src="/calendario/calendar-setup.js"></script>
'; ?>

<!-- InstanceEndEditable -->

</head>  
<body>
<br />
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="marco">
  <tr>
    <td colspan="3" align="left" background="/imagenes/fondo_admin.jpg" class="subtituloWeb3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="42%"><img src="/imagenes/logo.jpg" width="400" height="122" /></td>
          <td width="56%" align="right" valign="middle" class="normalContenido2">Panel Central de Utilidades - <span class="subtituloWeb3">Usuario:</span> <?php echo $this->_tpl_vars['nombre']; ?>
 <?php echo $this->_tpl_vars['apellido']; ?>
 <img src="/imagenes/user.png" width="30" height="30" align="absmiddle" /><br />
            <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="597" height="48">
              <param name="movie" value="/swf/redes_hora.swf" />
              <param name="quality" value="high" />
              <param name="wmode" value="transparent" />
              <param name="swfversion" value="6.0.65.0" />
              <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don�t want users to see the prompt. -->
              <param name="expressinstall" value="/Scripts/expressInstall.swf" />
              <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
              <!--[if !IE]>-->
              <object type="application/x-shockwave-flash" data="/swf/redes_hora.swf" width="597" height="48">
                <!--<![endif]-->
                <param name="quality" value="high" />
                <param name="wmode" value="transparent" />
                <param name="swfversion" value="6.0.65.0" />
                <param name="expressinstall" value="/Scripts/expressInstall.swf" />
                <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                <div>
                  <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                  <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                </div>
                <!--[if !IE]>-->
              </object>
              <!--<![endif]-->
          </object></td>
          <td width="2%" align="right" valign="middle" class="normalContenido2">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
 
  <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
    <td colspan="3"><!-- InstanceBeginEditable name="contenido" -->
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="normal">
  <tr>
    <th width="967" align="center" class="titulo"><img src="/imagenes/cuadros.png" width="14" height="14" align="left" /><?php echo $this->_tpl_vars['accion']; ?>
<img src="/imagenes/cuadritos.png" width="37" height="11" align="right" /></th>
  </tr>
  <tr>
    <td align="center" class="subtituloWeb3"><form action="" method="post" name="form1" id="form1" onsubmit="MM_validateForm('nombre','','R','apellido','','R','correo','','RisEmail','telefono','','NisNum','celular','','RisNum');return document.MM_returnValue">
                    <table width="56%" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td colspan="2" align="center" class="titulo_b">Datos Personales</td>
                        </tr>
                        <?php echo $this->_tpl_vars['mensaje']; ?>

                      <tr>
                        <td width="29%" align="left" class="subtituloWeb3">Nombres:</td>
                        <td width="71%" align="left" class="normalContenido"><input value="<?php echo $this->_tpl_vars['nombres']; ?>
" name="nombre" type="text" id="nombre" onkeypress="javascripts: return validarletras(event);" size="60" maxlength="20" />
                          *</td>
                      </tr>
                      <tr>
                        <td align="left" class="subtituloWeb3">Apellidos:</td>
                        <td align="left" class="normalContenido"><input value="<?php echo $this->_tpl_vars['apellidos']; ?>
" name="apellido" type="text" id="apellido" onkeypress="javascripts: return validarletras(event);" size="60" maxlength="20" />
                          *</td>
                      </tr>

                      <tr>
                        <td align="left" class="subtituloWeb3">E-mail:</td>
                        <td align="left" class="normalContenido"><input value="<?php echo $this->_tpl_vars['correo']; ?>
" name="correo" type="text" id="correo" onkeypress="javascripts: return validarcorreo(event);" size="60" maxlength="60" />
                          *</td>
                      </tr>
                      <tr>
                        <td align="left" class="subtituloWeb3">Tel&eacute;fono Local:</td>
                        <td align="left" class="normalContenido"><input value="<?php echo $this->_tpl_vars['telefono']; ?>
" onkeypress="javascripts: return validarnum(event);" name="telefono" type="text" id="telefono" size="60" maxlength="12" /></td>
                      </tr>
                      <tr>
                        <td align="left" class="subtituloWeb3">Celular:</td>
                        <td align="left" class="normalContenido"><input value="<?php echo $this->_tpl_vars['celular']; ?>
" onkeypress="javascripts: return validarnum(event);" name="celular" type="text" id="celular" size="60" maxlength="12" />
                          *</td>
                      </tr>
                      <tr>
                        <td align="left" class="subtituloWeb3">Ciudad:</td>
                        <td align="left"><input value="<?php echo $this->_tpl_vars['ciudad']; ?>
" onkeypress="javascripts: return validarletras(event);" name="ciudad" type="text" id="ciudad" size="60" maxlength="50" /></td>
                      </tr>
                      <tr>
                        <td colspan="2" align="center" class="titulo_b">Datos de Sistema</td>
                        </tr>
                      <tr>
                        <td align="left" class="subtituloWeb3" >&iquest;Deseas recibir nuestro bolet&iacute;n                                   electr&oacute;nico?</td>
                        <td align="left" class="normalContenido">S&iacute;
              <input <?php if ($this->_tpl_vars['publicidad'] == "" || $this->_tpl_vars['publicidad'] == 'si'): ?>checked="checked"<?php endif; ?> name="publicidad" type="radio" id="radio" value="si"  />
              No                          
              <input <?php if ($this->_tpl_vars['publicidad'] == 'no'): ?>checked="checked"<?php endif; ?> type="radio" name="publicidad" id="radio2" value="no" /></td>
                      </tr>
                      <tr>
                        <td align="left" class="subtituloWeb3">&iquest;C&oacute;mo nos conoci&oacute;?</td>
                        <td align="left" class="normalContenido"><select name="medio" id="medio">
                          <option <?php if ($this->_tpl_vars['medio'] == 'Google'): ?> selected='selected' <?php endif; ?> value="Google">B&uacute;squeda en Google</option>
                          <option <?php if ($this->_tpl_vars['medio'] == 'Facebook'): ?> selected='selected' <?php endif; ?> value="Facebook">Facebook</option>
                          <option <?php if ($this->_tpl_vars['medio'] == 'Radio'): ?> selected='selected' <?php endif; ?> value="Radio">Radio</option>
                          <option <?php if ($this->_tpl_vars['medio'] == 'Amigo'): ?> selected='selected' <?php endif; ?> value="Amigo">Referencia de un Amigo</option>
                          <option <?php if ($this->_tpl_vars['medio'] == 'Periodico'): ?> selected='selected' <?php endif; ?> value="Periodico">Nuestro Peri&oacute;dico</option>
                          <option <?php if ($this->_tpl_vars['medio'] == 'Volantes'): ?> selected='selected' <?php endif; ?> value="Volantes">Volantes Publicitarios</option>
                          <option <?php if ($this->_tpl_vars['medio'] == 'Otros'): ?> selected='selected' <?php endif; ?> value="Otros">Otros Medios</option>
                        </select></td>
                      </tr>
                      <tr>
                        <td align="right">&nbsp;</td>
                        <td align="left"><input onclick="javascript: return validarcombos3();" type="submit" name="envio" id="envio" value="Actualizar" /></td>
                      </tr>
</table>
                                    </form>
   
    </td>
    </tr>
</table>
<!-- InstanceEndEditable --></td>
  </tr>
    <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
  <td width="25%" align="center"><a href="/admin/panel_central.php">Panel Central <img src="/imagenes/atras.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
    <td width="50%" align="center"><!-- InstanceBeginEditable name="insetar" --><a href="/admin/registro">&nbsp;Volver <img src="/imagenes/atras.png" width="25" height="25" border="0" align="absmiddle" /></a><!-- InstanceEndEditable --></td>
    <td width="25%" align="center"><a href="/admin/cerrar_session.php">Cerrar Sesi&oacute;n <img src="/imagenes/cerrar.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"class="pie">
    Sigo S.A. / Copyright&copy; 2012 Todos los Derechos Reservados - Venezuela </td>
  </tr>
</table>
<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
</body>
<!-- InstanceEnd --></html>