<?php /* Smarty version 2.6.9, created on 2016-06-07 21:00:30
         compiled from admin/banner/formulario.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin/banner/formulario.tpl', 99, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Sigo S.A. - Panel Administrativo</title>
<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico"> 
<script type="text/javascript" language="javascript" src="/js/validar.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<!-- InstanceBeginEditable name="head" -->
<?php echo '
<link rel="stylesheet" type="text/css" media="all" href="/calendario/calendar-blue.css" title="blue" />
<script type="text/javascript" src="/calendario/calendar.js"></script>
<script type="text/javascript" src="/calendario/lang/calendar-en.js"></script>
<script type="text/javascript" src="/calendario/calendar-setup.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckfinder/ckfinder.js"></script>
<script type="text/javascript">
	window.onload = function()
	{
		var editor = CKEDITOR.replace( \'contenido\',
			{
		toolbar :
			[
            		[\'Font\',\'FontSize\',\'TextColor\',\'RemoveFormat\',\'-\',\'Table\',\'Image\',\'Source\',\'Templates\' ],\'/\',

            		[\'Bold\', \'Italic\',\'Underline\', \'-\', \'NumberedList\', \'BulletedList\', \'-\', \'Link\',\'Unlink\'],[\'JustifyLeft\',\'JustifyCenter\',\'JustifyRight\',\'JustifyBlock\'],[\'Maximize\', \'ShowBlocks\']

        		]
 
    		}
		);
		CKFinder.setupCKEditor( editor, \'../../ckfinder/\' ) ;
	};
</script>
	
'; ?>

<!-- InstanceEndEditable -->

</head>  
<body>
<br />
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="marco">
  <tr>
    <td colspan="3" align="left" background="/imagenes/fondo_admin.jpg" class="subtituloWeb3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="42%"><img src="/imagenes/logo.jpg" width="400" height="122" /></td>
          <td width="56%" align="right" valign="middle" class="normalContenido2">Panel Central de Utilidades - <span class="subtituloWeb3">Usuario:</span> <?php echo $this->_tpl_vars['nombre']; ?>
 <?php echo $this->_tpl_vars['apellido']; ?>
 <img src="/imagenes/user.png" width="30" height="30" align="absmiddle" /><br />
            <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="597" height="48">
              <param name="movie" value="/swf/redes_hora.swf" />
              <param name="quality" value="high" />
              <param name="wmode" value="transparent" />
              <param name="swfversion" value="6.0.65.0" />
              <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don�t want users to see the prompt. -->
              <param name="expressinstall" value="/Scripts/expressInstall.swf" />
              <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
              <!--[if !IE]>-->
              <object type="application/x-shockwave-flash" data="/swf/redes_hora.swf" width="597" height="48">
                <!--<![endif]-->
                <param name="quality" value="high" />
                <param name="wmode" value="transparent" />
                <param name="swfversion" value="6.0.65.0" />
                <param name="expressinstall" value="/Scripts/expressInstall.swf" />
                <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                <div>
                  <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                  <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                </div>
                <!--[if !IE]>-->
              </object>
              <!--<![endif]-->
          </object></td>
          <td width="2%" align="right" valign="middle" class="normalContenido2">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
 
  <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
    <td colspan="3"><!-- InstanceBeginEditable name="contenido" -->
      <form action="" method="post" enctype="multipart/form-data" name="form1" id="form1" onsubmit="MM_validateForm('etiqueta','','R');return document.MM_returnValue">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="normal">
          <tr>
            <th width="967" align="center"><span class="titulo"><img src="/imagenes/cuadros.png" width="14" height="14" align="left" /></span><?php echo $this->_tpl_vars['accion']; ?>
<span class="titulo"><img src="/imagenes/cuadritos.png" width="37" height="11" align="right" /></span></th>
          </tr>
          <tr>
            <td align="left" class="tituloWeb"><table width="60%" border="0" align="center" cellpadding="0" cellspacing="0">
            <?php echo $this->_tpl_vars['mensaje']; ?>

                <tr>
                  <td align="left" class="subtituloWeb3">Enlace:</td>
                  <td>
               <select name="categoria" id="categoria" onchange="javascripts: document.form1.submit();">
              <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['listado']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                <option <?php if ($this->_tpl_vars['categoria'] == $this->_tpl_vars['listado'][$this->_sections['i']['index']]['nombre_cat'] || $this->_tpl_vars['categoria'] == $this->_tpl_vars['listado'][$this->_sections['i']['index']]['id_cat']): ?> selected='selected'<?php endif; ?> value="<?php echo $this->_tpl_vars['listado'][$this->_sections['i']['index']]['id_cat']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['listado'][$this->_sections['i']['index']]['nombre_cat'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'htmlall') : smarty_modifier_escape($_tmp, 'htmlall')); ?>
</option> 
              <?php endfor; endif; ?>
              </select>
                  </td>
                </tr>
                <tr>
                  <td align="left" class="subtituloWeb3">Subenlace:</td>
                  <td><select name="subcategoria" id="subcategoria">
    			<option value="1">Ninguno(Principal)</option>
              <?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['listado2']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>   
                <option <?php if ($this->_tpl_vars['subcategoria'] == $this->_tpl_vars['listado2'][$this->_sections['j']['index']]['nombre_sub']): ?>selected='selected'<?php endif; ?> value="<?php echo $this->_tpl_vars['listado2'][$this->_sections['j']['index']]['id_sub']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['listado2'][$this->_sections['j']['index']]['nombre_sub'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'htmlall') : smarty_modifier_escape($_tmp, 'htmlall')); ?>
</option>
              <?php endfor; endif; ?>
              </select>
                  <?php echo $this->_tpl_vars['subcategoria']; ?>

                  
                  </td>
                </tr>
                <tr>
                <td width="90" align="left" class="subtituloWeb3">Etiqueta:</td>
                <td width="405"><input name="etiqueta" type="text" id="etiqueta" value="<?php echo $this->_tpl_vars['etiqueta']; ?>
" size="71" maxlength="100" />
                *</td>
              </tr>
              <tr>
                <td align="left" class="subtituloWeb3">Efecto:</td>
                <td><select name="efecto" id="efecto">
<option value="showBars" <?php if ($this->_tpl_vars['efecto'] == 'showBars'): ?> selected='selected' <?php endif; ?>>Show Bars</option>
<option value="cube" <?php if ($this->_tpl_vars['efecto'] == 'cube'): ?> selected='selected' <?php endif; ?>>Cube</option>
<option value="showBarsRandom" <?php if ($this->_tpl_vars['efecto'] == 'showBarsRandom'): ?> selected='selected' <?php endif; ?>>Show Bars Random</option>
<option value="tube" <?php if ($this->_tpl_vars['efecto'] == 'tube'): ?> selected='selected' <?php endif; ?>>Tube</option>
<option value="cubeStop" <?php if ($this->_tpl_vars['efecto'] == 'cubeStop'): ?> selected='selected' <?php endif; ?>>Cube Stop</option>
<option value="cubeHide" <?php if ($this->_tpl_vars['efecto'] == 'cubeHide'): ?> selected='selected' <?php endif; ?>>Cube Hide</option>
<option value="cubeSize" <?php if ($this->_tpl_vars['efecto'] == 'cubeSize'): ?> selected='selected' <?php endif; ?>>Cube Size</option>
<option value="horizontal" <?php if ($this->_tpl_vars['efecto'] == 'horizontal'): ?> selected='selected' <?php endif; ?>>Horizontal</option>
<option value="cubeRandom" <?php if ($this->_tpl_vars['efecto'] == 'cubeRandom'): ?> selected='selected' <?php endif; ?>>Cube Random</option>
<option value="fade" <?php if ($this->_tpl_vars['efecto'] == 'fade'): ?> selected='selected' <?php endif; ?>>Fade</option>
<option value="fadeFour" <?php if ($this->_tpl_vars['efecto'] == 'fadeFour'): ?> selected='selected' <?php endif; ?>>Fade Four</option>
<option value="paralell" <?php if ($this->_tpl_vars['efecto'] == 'paralell'): ?> selected='selected' <?php endif; ?>>Paralell</option>
<option value="blind" <?php if ($this->_tpl_vars['efecto'] == 'blind'): ?> selected='selected' <?php endif; ?>>Blind</option>
<option value="blindHeight" <?php if ($this->_tpl_vars['efecto'] == 'blindHeight'): ?> selected='selected' <?php endif; ?>>Blind Height</option>
<option value="directionLeft" <?php if ($this->_tpl_vars['efecto'] == 'directionLeft'): ?> selected='selected' <?php endif; ?>>Direction Left</option>
<option value="directionTop" <?php if ($this->_tpl_vars['efecto'] == 'directionTop'): ?> selected='selected' <?php endif; ?>>Direction Top</option>
<option value="directionBottom" <?php if ($this->_tpl_vars['efecto'] == 'directionBottom'): ?> selected='selected' <?php endif; ?>>Direction Bottom</option>
<option value="directionRight" <?php if ($this->_tpl_vars['efecto'] == 'directionRight'): ?> selected='selected' <?php endif; ?>>Direction Right</option>
                </select>
                  *</td>
              </tr>
              <tr>
                <td align="left" class="subtituloWeb3">Vinculo:</td>
                <td class="normalContenido"><input name="vinculo" type="text" id="vinculo" value="<?php echo $this->_tpl_vars['vinculo']; ?>
" size="71" maxlength="200" /></td>
              </tr>
              <tr>
                <td align="left" class="subtituloWeb3">Imagen:</td>
                <td class="normalContenido"><input type="file" name="documento" id="documento" />                  
                  *                  
                  (1 Mb m&aacute;x)</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td class="normalContenido"><input name="envio" type="submit" id="envio" onclick="javascript: return validarcontenido('&iquest;Est&aacute; seguro que desea guardar?');" value="Guardar"/>
                  &nbsp;
                  <input type="button" name="Submit" value="Cancelar" onclick="javascripts: location.href='/admin/banner'" /> 
                  (*) Datos Obligatorios</td>
              </tr>
            </table></td>
          </tr>
        </table>
      </form>
      
    <!-- InstanceEndEditable --></td>
  </tr>
    <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
  <td width="25%" align="center"><a href="/admin/panel_central.php">Panel Central <img src="/imagenes/atras.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
    <td width="50%" align="center"><!-- InstanceBeginEditable name="insetar" -->&nbsp;<!-- InstanceEndEditable --></td>
    <td width="25%" align="center"><a href="/admin/cerrar_session.php">Cerrar Sesi&oacute;n <img src="/imagenes/cerrar.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"class="pie">
    Sigo S.A. / Copyright&copy; 2012 Todos los Derechos Reservados - Venezuela </td>
  </tr>
</table>
<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
</body>
<!-- InstanceEnd --></html>