<?php
/* header para Smarty */
require('config/setup.php');
$smarty = new objeto_smarty;
/*  Fin header para Smarty */

include_once ("config/class.login.php");
include_once ("config/class.link.php");
include_once ("config/class.contenido.php");
include_once ("config/class.categoria.php");
include_once ("config/class.publicidad.php");
include_once ("config/class.mapa.php");
include_once ("config/class.banner.php");
include_once ("config/class.noticia.php");
include_once ("config/class.galeria.php");
include_once("config/conexion.inc.php");
require('smarty/libs/SmartyPaginate.class.php');

session_start();
if(!isset($acceso))
	$acceso = new Auth;
if ($_POST){
	if ($_POST['enviar'] == "Login"){
		$acceso->asignar_consulta($_POST['login'],$_POST['clave']);
		$acceso->login2($acceso->login, $acceso->password);
	};
}
if ($_GET){
	if ($_GET['enviar'] == "Logout")
		$acceso->logout();
}

SmartyPaginate::disconnect();
// required connect
SmartyPaginate::connect();
// set items per page
SmartyPaginate::setLimit(10);
SmartyPaginate::setPrevText('Anterior');
SmartyPaginate::setNextText('Siguiente');
SmartyPaginate::setFirstText('Primer');
SmartyPaginate::setLastText('�ltimo');

if(isset($_GET['cont']) && $_GET['cont']!="") 
	$cont=$_GET['cont'];

if($cont==1){
	header("location: index.php	");
	exit();
}

if(!isset($banner))
	$banner= new Banner;
$banner->listar_banner_publica($cont);

if(!isset($link))
	$link= new Link;
$link->listar_link_menu("todo");
$link->mostrar_link_publico($cont);

if(!isset($enlaces_A))
	$enlaces_A= new Link();
$enlaces_A->listar_link_menu("arriba");

if(!isset($enlaces_B))
	$enlaces_B= new Link();
$enlaces_B->listar_link_menu("central");

if(!isset($enlaces_C))
	$enlaces_C= new Link();
$enlaces_C->listar_link_menu("abajo");

if(!isset($contenido))
	$contenido= new Contenido();
$contenido->listar_contenido_imagen($cont);
$data=$contenido->listado;

//print_r($contenido->listado);

if($contenido->mensaje!="si"){
	$mensaje2="<tr><td align='center' colspan='2' class='error'>No existen registros en esta secci�n</td></tr>";	
}

if(!isset($sublink))
	$sublink= new Link();
$sublink->cargar_sublink();

function get_db_results($_data) {
	SmartyPaginate::setTotal(count($_data));
	return array_slice($_data, SmartyPaginate::getCurrentIndex(),
		SmartyPaginate::getLimit());
}

SmartyPaginate::setUrl("contenido.php?cont=".$_GET['cont']);

if(!isset($publicidad))
	$publicidad= new Publicidad;
$publicidad->cargar_publicidad("Banner Izquierdo");
$smarty->assign("publicidad", $publicidad->listado);

if(!isset($publicidad2))
	$publicidad2= new Publicidad;
$publicidad2->cargar_publicidad("Banner Derecho");
$smarty->assign("publicidad2", $publicidad2->listado);

if(!isset($imagenes))
	$imagenes= new Galeria;
$imagenes->mostrar_imagenes3("contenido", $contenido->listado[0]['id_con']);
$smarty->assign('imagenes',$imagenes->listado);

$mapa=new Mapa;
$mapa->listar_mapa_publica($contenido->listado[0]['id_con']);
if($mapa->mensaje!="si"){
	$mensaje="<tr><td colspan='4' align='center' class='error'>No hay punto en el mapa definido!</td></tr>";	
}

//print_r($mapa->listado);

if(isset($mapa->listado) && $mapa->listado!=""){
	$i=1;
	$baches="[";
	foreach($mapa->listado as $valor => $indice){
		if($i!=1) $baches.=", ";
		$baches.="['".$mapa->listado[$valor]['id_map']."', ".$mapa->listado[$valor]['latitud_map'].", ".$mapa->listado[$valor]['longitud_map'].", ".$i.", '".$mapa->listado[$valor]['directorio_image']."']";
		$i++;
	}
	$baches.= "];";
}

$total=$i;

$latitud=$mapa->listado[0]['latitud_map'];
$longitud=$mapa->listado[0]['longitud_map'];

if($cont=="10" || $cont=="11" || $cont=="13")
	$temp="map2";
else
	$temp="map";

$mapas="<script>
      function initialize() {
        var mapOptions = {
          zoom: 16,
          center: new google.maps.LatLng(".$latitud.", ".$longitud."),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('$temp'),
                                      mapOptions);

        setMarkers(map, beaches);
      }
      var beaches = ".$baches."

      function setMarkers(map, locations) {";
		
      for ($i = 0; $i < $total; $i++) {
			
	$mapas.="var image = new google.maps.MarkerImage('/imagenes/'+locations[".$i."][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/'+locations[".$i."][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var beach = locations[".$i."];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker".$i." = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});";
      }
	  
	  $mapas.="} google.maps.event.addDomListener(window, 'load', initialize);
    </script>";
	
/* footer para Smarty */
$smarty->assign("logo", $acceso->logo);
$smarty->assign("mapas", $mapas);
$smarty->assign("categoria", $nombre);
$smarty->assign("mensaje", $mensaje);
$smarty->assign("mensaje2", $mensaje2);
$smarty->assign("cont", $cont);
$smarty->assign("enlaces", $link->listado);
$smarty->assign('enlaces_A',$enlaces_A->listado);
$smarty->assign('enlaces_B',$enlaces_B->listado);
$smarty->assign('enlaces_C',$enlaces_C->listado);
$smarty->assign("banner", $banner->listado);

$smarty->assign("enlaces", $link->listado);
$smarty->assign('subcategorias',$sublink->listado);
if(isset($data) && $data!=""){
	$smarty->assign('contenido', get_db_results($data));
	// assign {$paginate} var
	SmartyPaginate::assign($smarty);
	// display results
}

$smarty->assign("accion", $link->etiqueta);
$smarty->assign("descripcion", $link->descripcion);
$smarty->assign("claves", $link->claves);

$smarty->force_compile=true;
if($cont=="10" || $cont=="11" || $cont=="13")
	$smarty->display('contenido3.tpl');
else if($cont=="13")
	$smarty->display('contenido2.tpl');
else if($cont=="20")
	$smarty->display('contenido4.tpl');
else if($cont=="19")
	$smarty->display('contenido5.tpl');
else
	$smarty->display('contenido.tpl');
/* Fin footer para Smarty */
?>