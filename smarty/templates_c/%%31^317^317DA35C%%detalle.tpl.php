<?php /* Smarty version 2.6.9, created on 2017-01-19 19:25:52
         compiled from admin/contenido/detalle.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Sigo S.A. - Panel Administrativo</title>
<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico"> 
<script type="text/javascript" language="javascript" src="/js/validar.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<!-- InstanceBeginEditable name="head" -->
<?php echo '
<script type="text/javascript" src="/js/prototype.js"></script>
<script type="text/javascript" src="/js/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="/js/lightbox.js"></script>
<link rel="stylesheet" href="/css/lightbox.css" type="text/css" media="screen" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=places&sensor=true"></script>
    
'; ?>

<!-- InstanceEndEditable -->

</head>  
<body>
<br />
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="marco">
  <tr>
    <td colspan="3" align="left" background="/imagenes/fondo_admin.jpg" class="subtituloWeb3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="42%"><img src="/imagenes/logo.jpg" width="400" height="122" /></td>
          <td width="56%" align="right" valign="middle" class="normalContenido2">Panel Central de Utilidades - <span class="subtituloWeb3">Usuario:</span> <?php echo $this->_tpl_vars['nombre']; ?>
 <?php echo $this->_tpl_vars['apellido']; ?>
 <img src="/imagenes/user.png" width="30" height="30" align="absmiddle" /><br />
            <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="597" height="48">
              <param name="movie" value="/swf/redes_hora.swf" />
              <param name="quality" value="high" />
              <param name="wmode" value="transparent" />
              <param name="swfversion" value="6.0.65.0" />
              <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don�t want users to see the prompt. -->
              <param name="expressinstall" value="/Scripts/expressInstall.swf" />
              <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
              <!--[if !IE]>-->
              <object type="application/x-shockwave-flash" data="/swf/redes_hora.swf" width="597" height="48">
                <!--<![endif]-->
                <param name="quality" value="high" />
                <param name="wmode" value="transparent" />
                <param name="swfversion" value="6.0.65.0" />
                <param name="expressinstall" value="/Scripts/expressInstall.swf" />
                <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                <div>
                  <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                  <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                </div>
                <!--[if !IE]>-->
              </object>
              <!--<![endif]-->
          </object></td>
          <td width="2%" align="right" valign="middle" class="normalContenido2">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
 
  <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
    <td colspan="3"><!-- InstanceBeginEditable name="contenido" -->
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="normal">
  <tr>
    <th width="762" align="center" class="titulo"><img src="/imagenes/cuadros.png" width="14" height="14" align="left" /><?php echo $this->_tpl_vars['accion']; ?>
<img src="/imagenes/cuadritos.png" width="37" height="11" align="right" /></th>
  </tr>
  <tr>
    <td align="center">
    <table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="60%" valign="top">
        <table width="90%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td align="left" class="subtituloWeb3">T&iacute;tulo:</td>
            <td class="adminContenido"><?php echo $this->_tpl_vars['nombres']; ?>
</td>
          </tr>
          <tr>
            <td align="left" class="subtituloWeb3">Enlace:</td>
            <td class="adminContenido"><?php echo $this->_tpl_vars['categoria']; ?>
</td>
            </tr>
          <tr>
            <td width="88" align="left" class="subtituloWeb3">Sunenlace:</td>
            <td width="477" class="adminContenido"><?php echo $this->_tpl_vars['subcategoria']; ?>
</td>
            </tr>
          <tr>
            <td align="left" class="subtituloWeb3">Fecha:</td>
            <td class="adminContenido"><?php echo $this->_tpl_vars['fecha']; ?>
 - <span class="subtituloWeb3">Hora:</span> <?php echo $this->_tpl_vars['hora']; ?>
</td>
            </tr>
          <tr>
            <td align="left" class="subtituloWeb3">Keywords:</td>
            <td class="adminContenido"><?php echo $this->_tpl_vars['claves']; ?>
</td>
          </tr>
          <tr>
            <td align="left" class="subtituloWeb3">Contenido:</td>
            <td class="adminContenido">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top"><?php echo $this->_tpl_vars['contenido']; ?>
</td>
            </tr>
          </table></td>
        <td width="40%" valign="top">
        <table width="100%" border="0" cellspacing="4" cellpadding="0">
        <tr>
                <td align="center" colspan="4" class="subtituloWeb3">Im&aacute;genes Asignadas</td>
                </tr>
      <?php $this->assign('cont', 0); ?>
      <?php if ($this->_tpl_vars['mensaje'] == ""): ?>
      <tr> <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['listado']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
        <td width="10%" align="center" class="normalContenido2" valign="top"><a href="/imagenes/<?php echo $this->_tpl_vars['listado'][$this->_sections['i']['index']]['directorio_image']; ?>
" title="<?php echo $this->_tpl_vars['listado'][$this->_sections['i']['index']]['nombre_image']; ?>
" rel="lightbox[roadtrip]" ><img border="0" src="/imagenes/<?php echo $this->_tpl_vars['listado'][$this->_sections['i']['index']]['directorio_image']; ?>
" width="150" class="fotos" /></a><br />
          <?php echo $this->_tpl_vars['listado'][$this->_sections['i']['index']]['nombre_image']; ?>
 <a onclick="javascript: return confirmar('&iquest;Esta seguro que desea borrar la imagen?');" href="imagen_borrar.php?id=<?php echo $this->_tpl_vars['listado'][$this->_sections['i']['index']]['id_image']; ?>
&amp;back=<?php echo $this->_tpl_vars['id']; ?>
&amp;tabla=contenido"><img border="0" src="/imagenes/eliminar.png" width="15" height="15" align="absmiddle" /></a></td>
        <?php $this->assign('cont', $this->_tpl_vars['cont']+1); ?>
        <?php if ($this->_tpl_vars['cont'] == 3): ?>
        <?php $this->assign('cont', 0); ?> </tr>
      <tr> <?php endif; ?>
        <?php endfor; endif; ?> </tr>
      <?php else: ?>
      <?php echo $this->_tpl_vars['mensaje']; ?>

      <?php endif; ?>
  <tr>
    <td colspan="5" align="center"><a href="imagen.php?id=<?php echo $this->_tpl_vars['id']; ?>
"><img src="/imagenes/dale.jpg" width="12" height="12" border="0" align="absmiddle" /> Insertar Imagen</a></td>
  </tr>
    </table></td>
      </tr>
      <?php if ($this->_tpl_vars['mapas'] != ""): ?>
      <tr>
        <td colspan="2" align="center">
        <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['mapas']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
        <div class="punto_mapa">
        	<div class="icono_mapa">
             <?php if ($this->_tpl_vars['mapas'][$this->_sections['i']['index']]['directorio_image'] == ""): ?>
             	<img src="/imagenes/bandera_sigo.png" width="100" height="100" />
             <?php else: ?>
             	<img src="/imagenes/<?php echo $this->_tpl_vars['mapas'][$this->_sections['i']['index']]['directorio_image']; ?>
" width="100" />
             <?php endif; ?>
             </div>
        	 <div class="latitud"><?php echo $this->_tpl_vars['mapas'][$this->_sections['i']['index']]['latitud_map']; ?>
</div>
             <div class="latitud"><?php echo $this->_tpl_vars['mapas'][$this->_sections['i']['index']]['longitud_map']; ?>
</div>
             <div class="latitud">
             <a href="/admin/mapa/editar.php?id=<?php echo $this->_tpl_vars['mapas'][$this->_sections['i']['index']]['id_map']; ?>
&amp;back=<?php echo $this->_tpl_vars['mapas'][$this->_sections['i']['index']]['contenido_map']; ?>
" title="Editar Punto"><img class="opacidad" border="0" src="/imagenes/editar.png" width="25" height="25" /></a>&nbsp;&nbsp; <a onclick="javascript: return confirmar('&iquest;Seguro desea eliminar este punto?')" href="/admin/mapa/eliminar.php?id=<?php echo $this->_tpl_vars['mapas'][$this->_sections['i']['index']]['id_map']; ?>
&amp;back=<?php echo $this->_tpl_vars['mapas'][$this->_sections['i']['index']]['contenido_map']; ?>
" title="Eliminar Punto"><img border="0" src="/imagenes/delete.png" class="opacidad" width="25" height="25" /></a></div>
        </div>
        <?php endfor; endif; ?>
        </td>
      </tr>
      <?php endif; ?>
      <tr>
      		<td colspan="2" align="center">
            <?php echo $this->_tpl_vars['mapas_estructura']; ?>

      <div id="map"></div>
      </td>
      </tr>
      <tr>
        <td colspan="2" align="center"><a href="/admin/mapa/insertar.php?back=<?php echo $this->_tpl_vars['id']; ?>
"><img src="/imagenes/dale.jpg" width="12" height="12" border="0" align="absmiddle" /> Insertar Punto</a></td>
      </tr>
    </table>
    
          </td>
  </tr>
</table>
<!-- InstanceEndEditable --></td>
  </tr>
    <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
  <td width="25%" align="center"><a href="/admin/panel_central.php">Panel Central <img src="/imagenes/atras.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
    <td width="50%" align="center"><!-- InstanceBeginEditable name="insetar" --><a href="/admin/contenido" title="Volver al Listado">&nbsp;Volver <img src="/imagenes/atras.png" width="25" height="25" border="0" align="absmiddle" /> </a> <a href="editar.php?id=<?php echo $this->_tpl_vars['id']; ?>
">&nbsp;Editar <img border="0" align="absmiddle" src="/imagenes/editar.png" width="25" height="25" /></a><!-- InstanceEndEditable --></td>
    <td width="25%" align="center"><a href="/admin/cerrar_session.php">Cerrar Sesi&oacute;n <img src="/imagenes/cerrar.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"class="pie">
    Sigo S.A. / Copyright&copy; 2012 Todos los Derechos Reservados - Venezuela </td>
  </tr>
</table>
<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
</body>
<!-- InstanceEnd --></html>