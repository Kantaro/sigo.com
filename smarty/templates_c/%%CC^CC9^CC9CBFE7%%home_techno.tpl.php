<?php /* Smarty version 2.6.9, created on 2012-05-17 13:55:03
         compiled from home_techno.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'home_techno.tpl', 151, false),array('function', 'paginate_prev', 'home_techno.tpl', 184, false),array('function', 'paginate_middle', 'home_techno.tpl', 184, false),array('function', 'paginate_next', 'home_techno.tpl', 184, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_techno.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title><?php echo $this->_tpl_vars['accion']; ?>
</title> 
<meta name="Title" content="<?php echo $this->_tpl_vars['accion']; ?>
" />
<meta name="Author" content="Alejandro D&iacute;az www.diazcreativos.net.ve" />
<meta name="Subject" content="Edge Squads Creando Tendencias" /> 
<meta name="Description" content="<?php echo $this->_tpl_vars['descripcion']; ?>
" /> 
<meta name="Keywords" content="<?php echo $this->_tpl_vars['claves']; ?>
" /> 
<meta name="Generator" content="DreamWeaver" />
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="7 days" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />

<link href="/css/estilos_techno.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico" />
<script src="/js/validar.js" type="text/javascript"></script>
<link href="/css/styles.css" type="text/css" media="all" rel="stylesheet" />
<link href="/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />
<link rel="stylesheet" href="/css/jshowoff.css" type="text/css" media="screen, projection" />
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery.skitter.js" type="text/javascript"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>

<script type="text/javascript" src="/js/jquery.jshowoff.min.js"></script>
<?php echo '
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push([\'_setAccount\', \'UA-6623041-14\']);
  _gaq.push([\'_trackPageview\']);

  (function() {
    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
    ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/javascript">
$(function(){

    // Skitter
    $(\'.box_skitter_large\').skitter();
    
});
</script>
'; ?>

<!-- InstanceBeginEditable name="head" -->
<?php echo '
<script type="text/javascript" src="/js/jquery.lightbox-0.5.js"></script>
<link rel="stylesheet" type="text/css" href="/css/jquery.lightbox-0.5.css" media="screen" />
<script type="text/javascript">
$(function() {
	$(\'#gallery a\').lightBox();
});
</script>
'; ?>

<!-- InstanceEndEditable -->
</head>
<body>
<div id="encabezado">
    <div id="topbanner"> 
        <div id="edge_squads"><img src="/imagenes/techno_squads.png" width="543" height="167" /></div>
    </div>
</div>
<div id="botonera_back">
	<div id="botonera">
    	<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
    	<div class="boton">
        <a href="<?php echo $this->_tpl_vars['enlaces'][$this->_sections['i']['index']]['etiqueta_cat']; ?>
_<?php echo $this->_tpl_vars['enlaces'][$this->_sections['i']['index']]['id_cat']; ?>
"><?php echo $this->_tpl_vars['enlaces'][$this->_sections['i']['index']]['nombre_cat']; ?>
</a></div>
        <?php endfor; endif; ?>
    </div> 
</div>

<div id="centro_back">
	<div id="banner">
    	<div id="rotador">
    	<div class="box_skitter box_skitter_large">  
			<ul>
                	<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['banner']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
					<li>
						<a <?php if ($this->_tpl_vars['banner'][$this->_sections['i']['index']]['vinculo_ban'] == ""): ?> href="#" <?php else: ?> href="<?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['vinculo_ban']; ?>
"<?php endif; ?>>
                        <img alt="<?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['etiqueta_ban']; ?>
" src="/imagenes/banner/<?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['url_ban']; ?>
" class="<?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['efecto_ban']; ?>
" /></a>
<div class="label_text">
							<p><?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['etiqueta_ban']; ?>
</p>
						</div>
					</li>
                    <?php endfor; endif; ?>
				</ul>
        </div>
        </div>
    	<div class="publicidad">
        	<div id="slidingFeatures3"> 
                    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['publicidad3']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <div title="<?php echo $this->_tpl_vars['publicidad3'][$this->_sections['i']['index']]['nombre_dir']; ?>
">
                    <a title="<?php echo $this->_tpl_vars['publicidad3'][$this->_sections['i']['index']]['nombre_dir']; ?>
" href="<?php echo $this->_tpl_vars['publicidad3'][$this->_sections['i']['index']]['url_dir']; ?>
" target="_blank">
                        <img border="0" src="/imagenes/publicidad/<?php echo $this->_tpl_vars['publicidad3'][$this->_sections['i']['index']]['directorio_dir']; ?>
" />
                    </a>
                    </div>
                    <?php endfor; endif; ?>
    			</div>
                <?php echo '
                <script type="text/javascript">		
                    $(document).ready(function(){ $(\'#slidingFeatures3\').jshowoff({
                        effect: \'fade\',
                        controls: false,
                        links: false,
                        cssClass: \'basicFeatures\',
                        hoverPause: true,
                        speed : 6000,
                        changespeed : 500
                    }); });
                </script>
                '; ?>

        </div>
    </div>
</div>

<div id="contenido_back">
  <div id="cont_centro">
  <!-- InstanceBeginEditable name="contenido" -->
          <div id="entradas">
          	<div id="caja_submodulos">
            	<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['subcategorias']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
         			<div class="submodulo">
           <a title="Ver Detalles" href="<?php echo $this->_tpl_vars['subcategorias'][$this->_sections['i']['index']]['etiqueta_cat']; ?>
_<?php echo $this->_tpl_vars['subcategorias'][$this->_sections['i']['index']]['etiqueta']; ?>
.<?php echo $this->_tpl_vars['subcategorias'][$this->_sections['i']['index']]['link_rel']; ?>
.<?php echo $this->_tpl_vars['subcategorias'][$this->_sections['i']['index']]['id_sub']; ?>
" class="noticia">			 <img  src="/imagenes/flechita_13.png" width="9" height="9" border="0" align="absmiddle" alt="Camara de Turismo Margarita" /> <?php echo $this->_tpl_vars['subcategorias'][$this->_sections['i']['index']]['nombre_sub']; ?>
</a>
           			</div>
                <?php endfor; endif; ?>
            </div>
            
            <?php if ($this->_tpl_vars['mensaje2'] == ""): ?>
            <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['contenido']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
            <div class="caja_registro">
                <?php if ($this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_image'] != ""): ?>
                <div class="caja_imagen">
                <a href="<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['url']; ?>
_<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_con']; ?>
_<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_cat']; ?>
" title="<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['nombre_image']; ?>
">
            <img class="fotos opacidad" src="/admin/galeria/publica_imagen.php?id=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_image']; ?>
&amp;anchura=180&amp;altura=135" /></a>
                </div>
                <?php endif; ?>
            	<div class="caja_contenido">
                    <div class="titulo"><?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['enlace_con']; ?>
: <?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['subenlace_con']; ?>
</div>
                    <div class="caja_titulo">
                    <img src="/imagenes/cuadritos_09.png" width="22" height="26" border="0" align="absmiddle" /> 
                    <a class="titulete" href="<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['url']; ?>
_<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_con']; ?>
_<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_cat']; ?>
" title="<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['nombre_con']; ?>
"><?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['nombre_con']; ?>
</a></div>
                    <div class="caja_detalles">
                    <?php echo ((is_array($_tmp=$this->_tpl_vars['contenido'][$this->_sections['i']['index']]['contenido_con'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 250, "(...)") : smarty_modifier_truncate($_tmp, 250, "(...)")); ?>

                    <a class="leermas" href="<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['url']; ?>
_<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_con']; ?>
_<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_cat']; ?>
" title="<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['nombre_con']; ?>
">Leer m�s &raquo;</a></div>
                    
                    <div id="like_twitter">
               		<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.neotendencias.net/contenido_detalle.php?id=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_con']; ?>
&cont=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_cat']; ?>
" data-text="#<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['enlace_con']; ?>
 | <?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['nombre_con']; ?>
" data-via="neotendencias" data-lang="es">Twittear</a><?php echo '
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>'; ?>

               </div>
                
               <div id="like_face">
               		<?php echo '
                    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>
					'; ?>

					<div class="fb-like" data-href="http://www.neotendencias.net/contenido_detalle.php?id=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_con']; ?>
&amp;cont=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_cat']; ?>
" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>
                </div>
                
        		</div>
            </div>
            <?php endfor; endif; ?>
            <?php else: ?>
            	<?php echo $this->_tpl_vars['mensaje2']; ?>

            <?php endif; ?>
            
            <div class="clear"></div>
            <?php if ($this->_tpl_vars['mensaje2'] == ""): ?>
            <div class="paginacion">
                                    <?php echo $this->_tpl_vars['paginate']['first']; ?>
-<?php echo $this->_tpl_vars['paginate']['last']; ?>
 de <?php echo $this->_tpl_vars['paginate']['total']; ?>
 Registros.  <?php echo smarty_function_paginate_prev(array(), $this);?>
 <?php echo smarty_function_paginate_middle(array(), $this);?>
 <?php echo smarty_function_paginate_next(array(), $this);?>
</div>
            
            <?php endif; ?>
      	</div>
          
          <div id="cont_derecho">
          	<div id="iconos_redes">
               <div class="logo_redes"><a href="http://www.facebook.com/neotendencias" target="_blank">
               <img src="/imagenes/redes_facebook.png" width="142" height="52" border="0" class="opacidad" /></a></div>
              <div class="logo_redes2"><a href="http://www.twitter.com/neotendencias" target="_blank">
              <img src="/imagenes/redes_twitter.png" width="142" height="52" border="0" class="opacidad" /></a></div>
            </div>
            
            <div class="publicidad2">
            	<div id="slidingFeatures2"> 
                    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['publicidad2']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <div title="<?php echo $this->_tpl_vars['publicidad2'][$this->_sections['i']['index']]['nombre_dir']; ?>
">
                    <a title="<?php echo $this->_tpl_vars['publicidad2'][$this->_sections['i']['index']]['nombre_dir']; ?>
" href="<?php echo $this->_tpl_vars['publicidad2'][$this->_sections['i']['index']]['url_dir']; ?>
" target="_blank">
                        <img border="0" src="/imagenes/publicidad/<?php echo $this->_tpl_vars['publicidad2'][$this->_sections['i']['index']]['directorio_dir']; ?>
" />
                    </a>
                    </div>
                    <?php endfor; endif; ?>
    			</div>
                <?php echo '
                <script type="text/javascript">		
                    $(document).ready(function(){ $(\'#slidingFeatures2\').jshowoff({
                        effect: \'fade\',
                        controls: false,
                        links: false,
                        cssClass: \'basicFeatures\',
                        hoverPause: true,
                        speed : 6000,
                        changespeed : 500
                    }); });
                </script>
                '; ?>

            </div>
            <div class="publicidad2">
            	<div id="slidingFeatures1"> 
                    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['publicidad1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <div title="<?php echo $this->_tpl_vars['publicidad1'][$this->_sections['i']['index']]['nombre_dir']; ?>
">
                    <a title="<?php echo $this->_tpl_vars['publicidad1'][$this->_sections['i']['index']]['nombre_dir']; ?>
" href="<?php echo $this->_tpl_vars['publicidad1'][$this->_sections['i']['index']]['url_dir']; ?>
" target="_blank">
                        <img border="0" src="/imagenes/publicidad/<?php echo $this->_tpl_vars['publicidad1'][$this->_sections['i']['index']]['directorio_dir']; ?>
" />
                    </a>
                    </div>
                    <?php endfor; endif; ?>
    			</div>
                <?php echo '
                <script type="text/javascript">		
                    $(document).ready(function(){ $(\'#slidingFeatures1\').jshowoff({
                        effect: \'fade\',
                        controls: false,
                        links: false,
                        cssClass: \'basicFeatures\',
                        hoverPause: true,
                        speed : 6000,
                        changespeed : 500
                    }); });
                </script>
                '; ?>

            </div>
          </div>
  <!-- InstanceEndEditable -->
  	<div class="clear"></div>
  </div>
</div>

<div id="pie_back">
    
  <div id="copy">
  	<div id="redes_mini">
    	<a href="https://twitter.com/neotendencias" class="twitter-follow-button" data-button="grey" data-text-color="#FFFFFF" data-link-color="#00AEFF">Follow @neotendencias</a>
<script src="//platform.twitter.com/widgets.js" type="text/javascript"></script>
    </div>
  	<div id="texto_pie">NeoTendencias / Copyright&copy; 2012 Todos los Derechos Reservados. Venezuela<br /> web: <a href="http://www.neotendencias.net">www.neotendencias.net</a> - email: <a href="mailto:info@neotendencias.net">info@neotendencias.net</a> 
    <br /><br />
          Dise&ntilde;o y Desarrollo: <a href="http://www.diazcreativos.net.ve" target="_blank"><img src="/imagenes/www.diazcreativos.net.ve.png" alt="D&iacute;az Creativos Desarrollo de P&aacute;ginas Web y Multimedia" width="30" height="30" border="0" align="middle" /> www.diazcreativos.net.ve </a>
    </div>
    <div class="clear"></div>
  </div>
</div> 
</body>
<!-- InstanceEnd --></html>