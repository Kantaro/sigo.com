<?php
/* header para Smarty */
require('config/setup.php');
$smarty = new objeto_smarty;
/*  Fin header para Smarty */
 
include_once ("config/class.login.php");
include_once ("config/class.link.php");
include_once ("config/class.contenido.php");
include_once ("config/class.galeria.php");
include_once ("config/class.categoria.php");
include_once ("config/class.publicidad.php");
include_once ("config/class.banner.php");
include_once("config/conexion.inc.php");
require('smarty/libs/SmartyPaginate.class.php');

session_start();
if(!isset($acceso))
	$acceso = new Auth;
if ($_POST){
	if ($_POST['enviar'] == "Enter"){
		$acceso->asignar_consulta($_POST['login'],$_POST['clave']);
		$acceso->login2($acceso->login, $acceso->password);
	}
	if ($_POST['enviar'] == "Logout")
		$acceso->logout();
}

if(isset($_GET['msg']) && $_GET['msg']==1){
	$mensaje="<tr><td align='center' colspan='2' class='error'>La sesi�n de usuario a caducado! ingrese de nuevo!</td></tr>";	
}else if(isset($_GET['msg']) && $_GET['msg']==2){
	$mensaje="<tr><td align='center' colspan='2' class='error'>Usted no posee privilegios pa entrar en esta �rea!</td></tr>";	
}else if($acceso->mensaje!=""){
	$mensaje="<tr><td align='center' colspan='2' class='error'>$acceso->mensaje</td></tr>";
}

if(isset($_GET['cont']) && $_GET['cont']!="")
	$cont=$_GET['cont'];

if($_GET['cont']==1 && $_GET['id']==1){
	header("location: mapa_completo.php");
	exit();
}else if($_GET['cont']==1 && $_GET['id']==2){
	header("location: revista.php");
	exit();
}else if($_GET['cont']==1 && $_GET['id']==3){
	header("location: sigoclub.php");
	exit();
}else if($_GET['cont']==1 && $_GET['id']==4){
	header("location: contenido.php?cont=19");
	exit();
}

//echo "Contenido: ".$cont." ID: ".$_GET['id'];

if(!isset($contenido))
	$contenido= new Contenido;
$contenido->mostrar_contenido();

if(!isset($imagenes))
	$imagenes= new Galeria;
$imagenes->mostrar_imagenes3("contenido", $_GET['cont']);

if($imagenes->mensaje!="si"){
	$mensaje2="<tr><td align='center' colspan='2'><span class='error'>No existen registros en esta secci�n</span></td></tr>";	
}

if(!isset($link))
	$link= new Link;
$link->listar_link_menu("todo");
$link->mostrar_link_publico($content);

if(!isset($enlaces_A))
	$enlaces_A= new Link();
$enlaces_A->listar_link_menu("arriba");

if(!isset($enlaces_B))
	$enlaces_B= new Link();
$enlaces_B->listar_link_menu("central");

if(!isset($enlaces_C))
	$enlaces_C= new Link();
$enlaces_C->listar_link_menu("abajo");

if(!isset($banner))
	$banner= new Banner;
$banner->listar_banner_publica($contenido->id_cat);

if(!isset($publicidad))
	$publicidad= new Publicidad;
$publicidad->cargar_publicidad("Banner Izquierdo");

$smarty->assign("publicidad", $publicidad->listado);

if(!isset($publicidad2))
	$publicidad2= new Publicidad;
$publicidad2->cargar_publicidad("Banner Derecho");

$smarty->assign("publicidad2", $publicidad2->listado);

function get_db_results($_data) {
	SmartyPaginate::setTotal(count($_data));
	return array_slice($_data, SmartyPaginate::getCurrentIndex(),
		SmartyPaginate::getLimit());
}

if(!isset($_GET['cont']) || $_GET['cont']=="")
	$cont=$contenido->id_cat;

/* footer para Smarty */
$smarty->assign('nombre_uso',$_SESSION['nombre_temporal']);
$smarty->assign('apellido_uso',$_SESSION['apellido_temporal']);
$smarty->assign("logo", $acceso->logo);
$smarty->assign('mensaje',$mensaje);
$smarty->assign('mensaje2',$mensaje2);
$smarty->assign("id", $contenido->id);
$smarty->assign("id_cat", $contenido->id_cat);
$smarty->assign("enlace", $contenido->enlace);
$smarty->assign("nombre", $contenido->nombre);
$smarty->assign("fecha", $contenido->fecha);
$smarty->assign("contenido", $contenido->contenido);
$smarty->assign("accion", $contenido->nombre);
$smarty->assign("descripcion", substr(strip_tags($contenido->contenido),0,200).' ...');
$smarty->assign("claves", $contenido->claves);

$smarty->assign("enlaces", $link->listado);
$smarty->assign('enlaces_A',$enlaces_A->listado);
$smarty->assign('enlaces_B',$enlaces_B->listado);
$smarty->assign('enlaces_C',$enlaces_C->listado);
$smarty->assign("banner", $banner->listado);
//print_r($categoria->listado);
// display results
$smarty->force_compile=true;
$smarty->display('contenido_detalle.tpl');

/* Fin footer para Smarty */
?>