<?php
/* header para Smarty */
require('config/setup.php');
$smarty = new objeto_smarty;
/*  Fin header para Smarty */
 
include_once ("config/class.login.php");
include_once ("config/class.link.php");
include_once ("config/class.contenido.php");
include_once ("config/class.galeria.php");
include_once ("config/class.noticia.php");
include_once ("config/class.categoria.php");
include_once ("config/class.publicidad.php");
include_once ("config/class.banner.php");
include_once("config/conexion.inc.php");
require('smarty/libs/SmartyPaginate.class.php');

session_start();
if(!isset($acceso))
	$acceso = new Auth;
if ($_POST){
	if ($_POST['enviar'] == "Enter"){
		$acceso->asignar_consulta($_POST['login'],$_POST['clave']);
		$acceso->login2($acceso->login, $acceso->password);
	}
	if ($_POST['enviar'] == "Logout")
		$acceso->logout();
}

if(isset($_GET['msg']) && $_GET['msg']==1){
	$mensaje="<tr><td align='center' colspan='2' class='error'>La sesi�n de usuario a caducado! ingrese de nuevo!</td></tr>";	
}else if(isset($_GET['msg']) && $_GET['msg']==2){
	$mensaje="<tr><td align='center' colspan='2' class='error'>Usted no posee privilegios pa entrar en esta �rea!</td></tr>";	
}else if($acceso->mensaje!=""){
	$mensaje="<tr><td align='center' colspan='2' class='error'>$acceso->mensaje</td></tr>";
}

if(!isset($banner))
	$banner= new Banner;
$banner->listar_banner_publica(1);

$content=1;

if(isset($_GET['cont'])) $content=$_GET['cont']; else $content=1;

if(!isset($contenido))
	$contenido= new Contenido();
$contenido->listar_contenido_imagen($content);

if($contenido->mensaje!="si"){
	$mensaje2="<div class='error'>No existen registros en esta secci�n</div>";
}

if(!isset($link))
	$link= new Link;
$link->listar_link_menu("todo");
$link->mostrar_link_publico($content);

if(!isset($enlaces_A))
	$enlaces_A= new Link();
$enlaces_A->listar_link_menu("arriba");

if(!isset($enlaces_B))
	$enlaces_B= new Link();
$enlaces_B->listar_link_menu("central");

if(!isset($enlaces_C))
	$enlaces_C= new Link();
$enlaces_C->listar_link_menu("abajo");

if(!isset($banner))
	$banner= new Banner;
$banner->listar_banner_publica($contenido->id_cat);

if(!isset($publicidad))
	$publicidad= new Publicidad;
$publicidad->cargar_publicidad("Banner Izquierdo");

$smarty->assign("publicidad", $publicidad->listado);

if(!isset($publicidad2))
	$publicidad2= new Publicidad;
$publicidad2->cargar_publicidad("Banner Derecho");

$smarty->assign("publicidad2", $publicidad2->listado);

if(!isset($noticia))
	$noticia= new Noticia();
$noticia->mostrar_noticia();
$noticia->listar_noticia_imagen();
$noticia->accion="Detalle de la Noticia";

/* footer para Smarty */
$smarty->assign('nombre_uso',$_SESSION['nombre_temporal']);
$smarty->assign('apellido_uso',$_SESSION['apellido_temporal']);
$smarty->assign("logo", $acceso->logo);
$smarty->assign('mensaje',$mensaje);
$smarty->assign('mensaje2',$mensaje2);
$smarty->assign("id", $contenido->id);
$smarty->assign("id_cat", $contenido->id_cat);
$smarty->assign("enlace", $contenido->enlace);

$smarty->assign("categoria", $noticia->categoria);
$smarty->assign("titulo", $noticia->titulo);
$smarty->assign("accion", $noticia->titulo);
$smarty->assign("subtitulo", $noticia->subtitulo);
$smarty->assign("iformacion", $noticia->contenido);
$smarty->assign("fecha", $noticia->fecha);
$smarty->assign("autor", $noticia->autor);
$smarty->assign('noticias',$noticia->listado);

$smarty->assign("descripcion", substr(strip_tags($noticia->contenido),0,200).' ...');
$smarty->assign("claves", $contenido->claves);

$smarty->assign('contenido', $contenido->listado);
$smarty->assign("banner", $banner->listado);
$smarty->assign("enlaces", $link->listado);
$smarty->assign('enlaces_A',$enlaces_A->listado);
$smarty->assign('enlaces_B',$enlaces_B->listado);
$smarty->assign('enlaces_C',$enlaces_C->listado);
//print_r($categoria->listado);
// display results
$smarty->force_compile=true;
$smarty->display('noticia_detalle.tpl');

/* Fin footer para Smarty */
?>