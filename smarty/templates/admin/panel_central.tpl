<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Sigo S.A. - Panel Administrativo</title>
<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico"> 
<script type="text/javascript" language="javascript" src="/js/validar.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->

</head>  
<body>
<br />
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="marco">
  <tr>
    <td colspan="3" align="left" background="/imagenes/fondo_admin.jpg" class="subtituloWeb3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="42%"><img src="/imagenes/logo.jpg" width="400" height="122" /></td>
          <td width="56%" align="right" valign="middle" class="normalContenido2">Panel Central de Utilidades - <span class="subtituloWeb3">Usuario:</span> {$nombre} {$apellido} <img src="/imagenes/user.png" width="30" height="30" align="absmiddle" /><br />
            <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="597" height="48">
              <param name="movie" value="/swf/redes_hora.swf" />
              <param name="quality" value="high" />
              <param name="wmode" value="transparent" />
              <param name="swfversion" value="6.0.65.0" />
              <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don�t want users to see the prompt. -->
              <param name="expressinstall" value="/Scripts/expressInstall.swf" />
              <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
              <!--[if !IE]>-->
              <object type="application/x-shockwave-flash" data="/swf/redes_hora.swf" width="597" height="48">
                <!--<![endif]-->
                <param name="quality" value="high" />
                <param name="wmode" value="transparent" />
                <param name="swfversion" value="6.0.65.0" />
                <param name="expressinstall" value="/Scripts/expressInstall.swf" />
                <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                <div>
                  <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                  <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                </div>
                <!--[if !IE]>-->
              </object>
              <!--<![endif]-->
          </object></td>
          <td width="2%" align="right" valign="middle" class="normalContenido2">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
 
  <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
    <td colspan="3"><!-- InstanceBeginEditable name="contenido" -->
      <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="normal">
        {$mensaje}
        <tr>
          <th colspan="5" align="center"><img src="/imagenes/cuadros.png" width="14" height="14" align="left" />Herramientas Administrativas<img src="/imagenes/cuadritos.png" width="37" height="11" align="right" /></th>
        </tr>
        <tr>
          <td width="20%" align="center" valign="middle" class="subtituloWeb3">Administradores</td>
          <td width="20%" align="center" valign="middle" class="subtituloWeb3">Eventos</td>
          <td width="20%" align="center" valign="middle" class="subtituloWeb3">Botonera</td>
          <td width="20%" align="center" valign="middle" class="subtituloWeb3">Contenido</td>
          <td width="20%" align="center" valign="middle" class="subtituloWeb3">Publicidad</td>
        </tr>
        <tr>
          <td align="center" valign="middle"><a href="/admin/usuario/"><img src="/imagenes/msn.png" width="60" height="60" border="0" /></a></td>
          <td align="center" valign="middle"><a href="/admin/evento/"><img src="/imagenes/calendar.png" width="60" height="60" border="0" /></a></td>
          <td align="center" valign="middle"><a href="/admin/link/"><img src="/imagenes/botonera.png" width="60" height="60" border="0" /></a></td>
          <td align="center" valign="middle"><a href="/admin/contenido/"><img src="/imagenes/mi_pedido.png" width="60" height="60" border="0" /></a></td>
          <td align="center" valign="middle"><a href="/admin/publicidad/"><img src="/imagenes/pedidos.png" width="60" height="60" border="0" /></a></td>
        </tr>
        <tr>
          <td align="center" valign="middle" class="subtituloWeb3">Noticia</td>
          <td align="center" valign="middle" class="subtituloWeb3">Categor&iacute;as</td>
          <td align="center" valign="middle" class="subtituloWeb3">Banner</td>
          <td align="center" valign="middle" class="subtituloWeb3">Registro</td>
          <td align="center" valign="middle" class="subtituloWeb3">Ayuda</td>
        </tr>
        <tr>
          <td align="center" valign="middle"><a href="/admin/noticia/"><img src="/imagenes/noticias.png" width="60" height="60" border="0" /></a></td>
          <td align="center" valign="middle"><a href="/admin/categoria"><img src="/imagenes/categoria.png" width="60" height="60" border="0" /></a></td>
          <td align="center" valign="middle"><a href="/admin/banner"><img src="/imagenes/evento.png" width="60" height="60" border="0" /></a></td>
          <td align="center" valign="middle"><a href="/admin/registro"><img src="/imagenes/registro.png" width="60" height="60" border="0" /></a></td>
          <td align="center" valign="middle"><a href="#"><img src="/imagenes/ayuda.png" alt="" width="60" height="60" border="0" /></a></td>
        </tr>
        <tr>
          <td align="center" valign="middle" class="subtituloWeb3">Revista Sigo</td>
          <td align="center" valign="middle" class="subtituloWeb3">&nbsp;</td>
          <td align="center" valign="middle" class="subtituloWeb3">&nbsp;</td>
          <td align="center" valign="middle" class="subtituloWeb3">&nbsp;</td>
          <td align="center" valign="middle" class="subtituloWeb3">&nbsp;</td>
        </tr>
        <tr>
          <td align="center" valign="middle"><a href="/admin/galeria"><img src="/imagenes/documentos.png" width="60" height="60" border="0" /></a></td>
          <td align="center" valign="middle">&nbsp;</td>
          <td align="center" valign="middle">&nbsp;</td>
          <td align="center" valign="middle">&nbsp;</td>
          <td align="center" valign="middle">&nbsp;</td>
        </tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
    <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
  <td width="25%" align="center"><a href="/admin/panel_central.php">Panel Central <img src="/imagenes/atras.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
    <td width="50%" align="center"><!-- InstanceBeginEditable name="insetar" -->&nbsp;<!-- InstanceEndEditable --></td>
    <td width="25%" align="center"><a href="/admin/cerrar_session.php">Cerrar Sesi&oacute;n <img src="/imagenes/cerrar.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"class="pie">
    Sigo S.A. / Copyright&copy; 2012 Todos los Derechos Reservados - Venezuela </td>
  </tr>
</table>
<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
</body>
<!-- InstanceEnd --></html>
