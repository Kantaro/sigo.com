136
a:4:{s:8:"template";a:1:{s:14:"contenido5.tpl";b:1;}s:9:"timestamp";i:1562877916;s:7:"expires";i:1562881516;s:13:"cache_serials";a:0:{}}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_ucs.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<title>Universidad Corporativa de Sigo | Sigo Venezuela</title>
<meta name="Title" content="Sigo S.A." />
<meta name="Author" content="Alejandro D&iacute;az http://diazcreativos.net.ve/" />
<meta name="Subject" content="Sigo S.A." />
<meta name="Description" content="Universidad Corporativa SIGO (UCS), un espacio para compartir información actualizada sobre ofertas de formación, actividades de extensión y eventos que se realizan continuamente, en pro de la formación de líderes de clase mundial." /> 
<meta name="Keywords" content="Universidad, eventos, congresos, corporativos, UCS sigo, margarita , venezuela" />

<meta name="Generator" content="DreamWeaver" /> 
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="7 days" /> 
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />

<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico" />
<script src="/js/validar.js" type="text/javascript"></script>
<link href="/css/skitter.styles3.css" type="text/css" media="all" rel="stylesheet" />
<link rel="stylesheet" href="/css/jshowoff.css" type="text/css" media="screen, projection" />
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery.skitter.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jshowoff.min.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.js"></script>
<link rel="stylesheet" type="text/css" href="/css/skin2.css" />
<link href="/css/styles.css" type="text/css" media="all" rel="stylesheet" />
<link href="/dropdown/menu.css" type="text/css" media="all" rel="stylesheet" />

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
		vertical: true,
        auto: 0,
		easing: 'easeOutBack',
		animation: 'slow',
		scroll: 3,
        wrap: 'circular',
        initCallback: mycarousel_initCallback,
		itemLoadCallback: itemLoadCallbackFunction
    });
});

$(function(){
    // Skitter
    $('.box_skitter_large').skitter();
    
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6623041-23']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- InstanceBeginEditable name="head" -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=places&sensor=true"></script>

<script type="text/javascript" src="/js/jquery.lightbox-0.5.js"></script>
<link rel="stylesheet" type="text/css" href="/css/jquery.lightbox-0.5.css" media="screen" />
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
});
</script>


<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('/imagenes/logo_twitter_color.png','/imagenes/logo_facebook_color.png','/imagenes/logo_youtube_color.png')">
<div id="main_principal"> 
<div id="encabezado">
        <div id="logopequeno"><a href="index.php" title="Home"><img src="/imagenes/logotipo_B.png" width="154" height="144" border="0" /></a></div>
        <div id="botonera_A">
            <ul id="menu">
                                        <li><a href="contenido.php?cont=1#next">Home</a>
                                            </li>
                	                    <li><a href="contenido.php?cont=2#next">HyperMarket</a>
                                                <div class="dropdown_3columns">
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=64#next">
                                Artículos del Hogar</a> -->
                                Artículos del Hogar
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=65#next">
                                Bebés</a> -->
                                Bebés
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=66#next">
                                Bodegón</a> -->
                                Bodegón
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=67#next">
                                Carnicería</a> -->
                                Carnicería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=68#next">
                                Charcutería</a> -->
                                Charcutería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=69#next">
                                Chucherías</a> -->
                                Chucherías
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=70#next">
                                Computación</a> -->
                                Computación
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=71#next">
                                Congelados</a> -->
                                Congelados
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=72#next">
                                Cuidado Personal</a> -->
                                Cuidado Personal
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=73#next">
                                Deportes</a> -->
                                Deportes
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=75#next">
                                Farmacia</a> -->
                                Farmacia
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=74#next">
                                Electrodomésticos</a> -->
                                Electrodomésticos
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=76#next">
                                Ferretería</a> -->
                                Ferretería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=77#next">
                                Frulever</a> -->
                                Frulever
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=78#next">
                                Lámparas</a> -->
                                Lámparas
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=79#next">
                                Lencería</a> -->
                                Lencería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=80#next">
                                Limpieza</a> -->
                                Limpieza
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=81#next">
                                Mascotas</a> -->
                                Mascotas
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=82#next">
                                Panadería</a> -->
                                Panadería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=83#next">
                                Papelería</a> -->
                                Papelería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=84#next">
                                Pescadería</a> -->
                                Pescadería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=85#next">
                                Pintura</a> -->
                                Pintura
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=86#next">
                                Sigo Car</a> -->
                                Sigo Car
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=87#next">
                                Ropa</a> -->
                                Ropa
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=88#next">
                                Sonido y Video</a> -->
                                Sonido y Video
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=2&sub=89#next">
                                Víveres</a> -->
                                Víveres
                                </div>
                                                    </div>
                                            </li>
                	                    <li><a href="contenido.php?cont=3#next">SuperMarket</a>
                                                <div class="dropdown_3columns">
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=43#next">
                                Bodegón</a> -->
                                Bodegón
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=19#next">
                                Carnicería</a> -->
                                Carnicería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=44#next">
                                Charcutería</a> -->
                                Charcutería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=45#next">
                                Chucherías</a> -->
                                Chucherías
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=46#next">
                                Congelados</a> -->
                                Congelados
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=22#next">
                                Cuidado Personal</a> -->
                                Cuidado Personal
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=21#next">
                                Farmacia</a> -->
                                Farmacia
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=18#next">
                                Frulever</a> -->
                                Frulever
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=49#next">
                                Limpieza</a> -->
                                Limpieza
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=47#next">
                                Mascotas</a> -->
                                Mascotas
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=33#next">
                                Panadería</a> -->
                                Panadería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=42#next">
                                Pescadería</a> -->
                                Pescadería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=3&sub=48#next">
                                Víveres</a> -->
                                Víveres
                                </div>
                                                    </div>
                                            </li>
                	                    <li><a href="contenido.php?cont=4#next">HomeMarket</a>
                                                <div class="dropdown_3columns">
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=50#next">
                                Artículos del Hogar</a> -->
                                Artículos del Hogar
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=51#next">
                                Bebés</a> -->
                                Bebés
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=52#next">
                                Computación</a> -->
                                Computación
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=53#next">
                                Deportes</a> -->
                                Deportes
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=54#next">
                                Electrodomésticos</a> -->
                                Electrodomésticos
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=56#next">
                                Ferretería</a> -->
                                Ferretería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=57#next">
                                Lámparas</a> -->
                                Lámparas
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=58#next">
                                Lencería</a> -->
                                Lencería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=59#next">
                                Papelería</a> -->
                                Papelería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=60#next">
                                Pintura</a> -->
                                Pintura
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=61#next">
                                Sigo Car</a> -->
                                Sigo Car
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=62#next">
                                Ropa</a> -->
                                Ropa
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=4&sub=63#next">
                                Sonido y Video</a> -->
                                Sonido y Video
                                </div>
                                                    </div>
                                            </li>
                	                    <li><a href="contenido.php?cont=5#next">MiniMarket</a>
                                                <div class="dropdown_3columns">
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=5&sub=91#next">
                                Chucherías</a> -->
                                Chucherías
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=5&sub=92#next">
                                Cuidado Personal</a> -->
                                Cuidado Personal
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=5&sub=90#next">
                                Farmacia</a> -->
                                Farmacia
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=5&sub=93#next">
                                Limpieza</a> -->
                                Limpieza
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=5&sub=94#next">
                                Mascotas</a> -->
                                Mascotas
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=5&sub=95#next">
                                Panadería</a> -->
                                Panadería
                                </div>
                                                            <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=5&sub=96#next">
                                Víveres</a> -->
                                Víveres
                                </div>
                                                    </div>
                                            </li>
                	                    <li><a href="contenido.php?cont=7#next">Bodegón</a>
                                            </li>
                	                    <li><a href="contenido.php?cont=8#next">Farmacia</a>
                                            </li>
                	            </ul>
        </div>
        <div id="topbanner2">
        	<div id="botonera_lateral">
            	<ul id="mycarousel" class="jcarousel jcarousel-skin-tango">
                                <li>
                	<div class="modulo_subcat">
                    <div class="foto_subcat">
                    <a href="contenido_sub.php?cont=19&amp;sub=120#next" title="$subcategorias[i].etiqueta_sub">
                    <img class="opacidad" border="0" src="/imagenes/icono_default.jpg" width="33" height="33" /></a>
             		</div>
               	    <div class="titulo_subcat">
                    <a title="UCS Focos de Acción" href="contenido_sub.php?cont=19&amp;sub=120#next">Focos de Acción</a></div>
                   </div>
              </li>
           	                    <li>
                	<div class="modulo_subcat">
                    <div class="foto_subcat">
                    <a href="contenido_sub.php?cont=19&amp;sub=121#next" title="$subcategorias[i].etiqueta_sub">
                    <img class="opacidad" border="0" src="/imagenes/icono_default.jpg" width="33" height="33" /></a>
             		</div>
               	    <div class="titulo_subcat">
                    <a title="Disponibilidad de Espacios " href="contenido_sub.php?cont=19&amp;sub=121#next">Espacios </a></div>
                   </div>
              </li>
           	                    <li>
                	<div class="modulo_subcat">
                    <div class="foto_subcat">
                    <a href="contenido_sub.php?cont=19&amp;sub=122#next" title="$subcategorias[i].etiqueta_sub">
                    <img class="opacidad" border="0" src="/imagenes/icono_default.jpg" width="33" height="33" /></a>
             		</div>
               	    <div class="titulo_subcat">
                    <a title="Eventos UCS" href="contenido_sub.php?cont=19&amp;sub=122#next">Eventos</a></div>
                   </div>
              </li>
           	                    </ul>
            </div>
        	<div class="box_skitter box_skitter_large">  
			<ul>
            	                						<li>
						<a  href="#" >
                        <img alt="Universidad Corporativa de Sigo" src="/imagenes/banner/1351099913.png" class="fade" /></a>
<div class="label_text">
							<p>Universidad Corporativa de Sigo</p>
					  </div>
					</li>
                                    			  </ul>
        	</div>
        </div>
    </div>
  <div class="clear"></div>
  <!-- InstanceBeginEditable name="contenido" --><br />
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left">
              <table width="100%" border="0" cellspacing="0" cellpadding="6">
             	                            <tr>
                        <td valign="top"  colspan="2" >
            	<span class="titulo">Universidad Corporativa de Sigo</span>  - <span class="normalContenido2">publicado:24/10/2012</span> <br />
<table border="0" cellpadding="5" cellspacing="0" style="width: 100%">
	<tbody>
		<tr>
			<td colspan="2">
				<p>
					<img alt="" src="/imagenes/galeria/images/ucs.jpg" style="width: 160px; float: left; height: 120px; margin-left: 6px; margin-right: 6px" />Bienvenidos al portal de la Universidad Corporativa SIGO (UCS), un espacio para compartir informaci&oacute;n actualizada sobre ofertas de formaci&oacute;n, actividades de extensi&oacute;n y eventos que se realizan continuamente, en pro de la formaci&oacute;n de l&iacute;deres de clase mundial.</p>
				<p>
					La UCS es, en esencia,&nbsp; un espacio de aprendizaje continuo, basado en el desarrollo de competencias t&eacute;cnicas, gerenciales, directivas y sobre todo, con una oferta que facilita el desarrollo de valores humanos integrales y sobre todo, compromiso con el desarrollo de nuestros colaboradores, las comunidades y su aporte a la conformaci&oacute;n de una ciudadan&iacute;a corporativa c&oacute;nsona con el crecimiento del pa&iacute;s.</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>
					<img alt="" src="/imagenes/galeria/images/fotos_ucs.jpg" style="margin: 6px; width: 311px; float: left; height: 345px" /></p>
			</td>
			<td style="vertical-align: top;">
				<p>
					Desde el a&ntilde;o 2006, inicia sus operaciones en el estado de Nueva Esparta, focalizando su acci&oacute;n en la formaci&oacute;n y desarrollo de los trabajadores de la empresa SIGO S.A., convirti&eacute;ndose en una oportunidad de nivelaci&oacute;n y especializaci&oacute;n para los trabajadores en materia de retail. La innovaci&oacute;n en sus dise&ntilde;os y la variedad de su oferta acad&eacute;mica no solo responde a las necesidades de la empresa. Con el tiempo, su acci&oacute;n se ha ampliado a las comunidades a trav&eacute;s de actividades y diplomados que promuevan su desarrollo en &aacute;reas de: Emprendimiento, Gesti&oacute;n Comunitaria, Liderazgo e Innovaci&oacute;n Social. <a href="http://www.4footballnews.com" style="color: rgba(190, 203, 208, 0.02);">football news</a></p>
				<p>
					En la actualidad, cuenta con tres Centros de Formaci&oacute;n a trav&eacute;s de los cuales se canaliza su oferta:</p>
				<ul>
					<li>
						Centro de Desarrollo Humano</li>
					<li>
						Centro de Conocimientos T&eacute;cnicos</li>
					<li>
						Centro de Formaci&oacute;n Gerencial y Directiva</li>
				</ul>
				<p>
					Cada uno de los centros interact&uacute;a de manera coordinada en la formaci&oacute;n de competencias transversales que faciliten el autodesarrollo, el aprender a aprender, desarrollo de la creatividad e innovaci&oacute;n, cultura de negocios, liderazgo, comunicaci&oacute;n y tecnolog&iacute;a. Para ello se han dise&ntilde;ado Diplomados, talleres, cursos, conferencias y charlas en algunos casos certificados en alianza con universidades nacionales e internacionales.</p>
				<p>
					De cara al futuro el reto es convertirnos en un centro de innovaci&oacute;n social que ofrezca adem&aacute;s del componente de formaci&oacute;n, herramientas y soluciones empresariales que faciliten la intervenci&oacute;n en el mundo de las organizaciones en el &aacute;rea de retail, as&iacute; como la especializaci&oacute;n en coach organizacional para equipos de alto nivel directivo.</p>
			</td>
		</tr>
	</tbody>
</table></td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="separadores"></td>
             </tr>
                        <tr>
                        <td valign="top"  colspan="2" >
            	<span class="titulo">Salón de Usos Múltiples</span>  - <span class="normalContenido2">publicado:04/03/2013</span> <br />
<p>
	<strong><font><font>- Kapasite:</font></font></strong><font><font> 150 kişi oturmuş.</font></font></p>
<p>
	<font><font>UCS y&uuml;ksek donanımlı 332 m2 boş alan sağlar - </font><font>kalite &ccedil;evre, ahşap d&ouml;şeme, geniş aydınlatma ses </font></font><em><font><font>,</font></font></em><font><font> b&uuml;y&uuml;k gruplar ya da dinlenme ve rahatlama i&ccedil;in stant sunumları montaj i&ccedil;in idealdir.&nbsp;</font></font></p>
<div style="position: absolute; left: -4168px">
	<a href="http://bedirogluevdenevenakliyat.com/" title="evden eve nakliyat">evden eve nakliyat</a> - <a href="http://bedirogluevdenevenakliyat.com/sehirlerarasi-evden-eve-nakliyat.html" title="şehirlararası evden eve nakliyat">şehirlerarası evden eve nakliyat</a> <a href="http://bedirogluevdenevenakliyat.com/esya-depolama.html" title="eşya depolama">eşya depolama</a> <a href="http://bedirogluevdenevenakliyat.com/istanbul/pendik.html" title="pendik evden eve nakliyat">pendik evden eve nakliyat</a> - <a href="http://bedirogluevdenevenakliyat.com/istanbul/kartal.html" title="kartal evden eve nakliyat">kartal evden eve nakliyat</a> <a href="http://bedirogluevdenevenakliyat.com/istanbul.html" title="http://bedirogluevdenevenakliyat.com/istanbul.html">http://bedirogluevdenevenakliyat.com/istanbul.html</a></div>
</td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="separadores"></td>
             </tr>
                                      </table>
              </td>
            </tr>
            <tr>
            	<td align="center"><script>
      function initialize() {
        var mapOptions = {
          zoom: 16,
          center: new google.maps.LatLng(10.95143, -63.870564),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('map'),
                                      mapOptions);

        setMarkers(map, beaches);
      }
      var beaches = [['117', 10.95143, -63.870564, 1, 'mapa/1351100806.png']];

      function setMarkers(map, locations) {var image = new google.maps.MarkerImage('/imagenes/'+locations[0][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/'+locations[0][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var beach = locations[0];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker0 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});var image = new google.maps.MarkerImage('/imagenes/'+locations[1][4],
	
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
       		var shadow = new google.maps.MarkerImage('/imagenes/'+locations[1][4],
            new google.maps.Size(70, 70),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 70));
			
        	var shape = {
				coord: [1, 1, 1, 70, 70, 70, 70 , 1],
				type: 'poly'
        	};
		
			var beach = locations[1];
			var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
			var marker1 = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});} google.maps.event.addDomListener(window, 'load', initialize);
    </script>
      						<div id="map"></div>
                         <br />
                </td>
            </tr>
          </table> 
        <!-- InstanceEndEditable -->
  <div id="menu_categorias">
  <div id="redes2">
   		<div class="redes_icono2"> <a href="http://twitter.com/sigosa"><img src="/imagenes/logo_twitter_gris.png" width="46" height="46" border="0" id="Image1" onmouseover="MM_swapImage('Image1','','/imagenes/logo_twitter_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.facebook.com/SigoVenezuela"><img src="/imagenes/logo_face_gris.png" width="46" height="46" border="0" id="Image2" onmouseover="MM_swapImage('Image2','','/imagenes/logo_facebook_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.youtube.com/SigoVenezuela"><img src="/imagenes/logo_youtube_gris.png" width="46" height="46" border="0" id="Image3" onmouseover="MM_swapImage('Image3','','/imagenes/logo_youtube_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
</div>
		
      		<div class="modulo">
            <div><a href="#" title="Sigo">
            <span class="titulo4">Sigo</span></a></div>
                                            <div class="submodulos">
                <a href="contenido_sub.php?cont=15&sub=97#next" title="La Empresa Sigo S.A." class="enlace_top">&bull; ¿Quiénes Somos?</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=15&sub=98#next" title="Misión y Visión Sigo S.A." class="enlace_top">&bull; Misión y Visión</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=15&sub=99#next" title="Sala de Prensa" class="enlace_top">&bull; Sala de Prensa</a></div>
                                    </div>
     		<div class="modulo">
            <div><a href="#" title="Sucursales">
            <span class="titulo4">Sucursales</span></a></div>
                                            <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=106#next" title="Sucursales de Porlamar" class="enlace_top">&bull; Porlamar</a></div>
                                <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=105#next" title="Sucursales de Pampatar" class="enlace_top">&bull; Pampatar</a></div>
                                <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=100#next" title="Sucursal de Barcelona" class="enlace_top">&bull; Barcelona</a></div>
                                <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=103#next" title="Sucursales de Maturín" class="enlace_top">&bull; Maturín</a></div>
                                <div class="submodulos2">
                <a href="contenido_sub.php?cont=16&sub=104#next" title="Sucursales de Paraguaná" class="enlace_top">&bull; Paraguaná</a></div>
                                    </div>
     		<div class="modulo">
            <div><a href="#" title="Corporativo">
            <span class="titulo4">Corporativo</span></a></div>
                                            <div class="submodulos">
                <a href="contenido_sub.php?cont=17&sub=109#next" title="Innova Centros Comerciales" class="enlace_top">&bull; Innova Centros Comerciales</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=17&sub=110#next" title="Intranet Sigo S.A." class="enlace_top">&bull; Intranet</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=17&sub=115#next" title="Empleo Sigo" class="enlace_top">&bull; Empleo</a></div>
                                    </div>
     		<div class="modulo">
            <div><a href="#" title="Pensando en Tí">
            <span class="titulo4">Pensando en Tí</span></a></div>
                                            <div class="submodulos">
                <a href="contenido_sub.php?cont=18&sub=111#next" title="Valoramos tu bienestar" class="enlace_top">&bull; Valoramos tu bienestar</a></div>
                                <div class="submodulos">
                <a href="contenido_sub.php?cont=18&sub=112#next" title="Eventos Sigo S.A." class="enlace_top">&bull; Eventos</a></div>
                                    </div>
     	
  </div>
</div>
<div class="clear"></div>

<div id="panel_pie">
    <div id="pie">
    <div id="botonera">
    	<div class="boton"><a href="index.php">Home</a></div>
                    <div class="boton"><a href="contenido.php?cont=9#next">Smilies</a></div>
                    <div class="boton"><a href="contenido.php?cont=20#next">Responsabilidad Social</a></div>
                    <div class="boton"><a href="contenido.php?cont=19#next">Universidad Corporativa de Sigo</a></div>
                <div class="boton"><a title="Contactos"  href="contacto.php">Cont&aacute;ctanos</a></div>
        <div class="boton_less">&nbsp;</div> 
    </div>
    <div id="logominimo"><img src="/imagenes/loguito.png" width="50" height="45" /></div>
    <div id="copy"> | SIGO S.A. &reg; 2016 RIF:J-08003048-6</div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>