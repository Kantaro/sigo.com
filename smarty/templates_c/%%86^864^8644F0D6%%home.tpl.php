<?php /* Smarty version 2.6.9, created on 2019-07-11 17:38:16
         compiled from home.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'home.tpl', 152, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_padre.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<title><?php echo $this->_tpl_vars['accion']; ?>
 | Sigo Venezuela</title>
<meta name="Title" content="Sigo S.A." />
<meta name="Author" content="Alejandro D&iacute;az http://diazcreativos.net.ve/" />
<meta name="Subject" content="Sigo S.A." />
<meta name="Description" content="<?php echo $this->_tpl_vars['descripcion']; ?>
" /> 
<meta name="Keywords" content="<?php echo $this->_tpl_vars['claves']; ?>
" />

<meta name="Generator" content="DreamWeaver" /> 
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="7 days" /> 
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />

<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico" />
<script src="/js/validar.js" type="text/javascript"></script>
<link href="/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />
<link rel="stylesheet" href="/css/jshowoff.css" type="text/css" media="screen, projection" />
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery.skitter.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jshowoff.min.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.js"></script>
<link rel="stylesheet" type="text/css" href="/css/skin.css" />
<link href="/css/styles.css" type="text/css" media="all" rel="stylesheet" />
<link href="/dropdown/menu.css" type="text/css" media="all" rel="stylesheet" />
<?php echo '
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery(\'#mycarousel\').jcarousel({
		vertical: true,
        auto: 4,
		easing: \'easeOutBack\',
		animation: \'slow\',
		scroll: 3,
        wrap: \'circular\',
        initCallback: mycarousel_initCallback,
		itemLoadCallback: itemLoadCallbackFunction
    });
});

$(function(){
    // Skitter
    $(\'.box_skitter_large\').skitter();
    
});
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push([\'_setAccount\', \'UA-6623041-23\']);
  _gaq.push([\'_trackPageview\']);

  (function() {
    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
    ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

'; ?>

<!-- InstanceBeginEditable name="head" -->
<?php echo '

'; ?>

<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('/imagenes/logo_twitter_color.png','/imagenes/logo_facebook_color.png','/imagenes/logo_youtube_color.png')">
<div id="main_principal">
<div id="encabezado">
        <div id="logopequeno"><a href="index.php" title="Home"><img src="/imagenes/logotipo_<?php echo $this->_tpl_vars['logo']; ?>
.png" width="154" height="144" border="0" /></a></div>
        <div id="botonera_A">
            <ul id="menu">
                    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_A']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <li><a href="contenido.php?cont=<?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['id_cat']; ?>
#next"><?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['nombre_cat']; ?>
</a>
                        <?php if ($this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces'] != ""): ?>
                        <div class="dropdown_3columns">
                            <?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
                                <div class="col_1">
                                <!-- <a href=  "contenido_sub.php?cont=<?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['id_cat']; ?>
&sub=<?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['id_sub']; ?>
#next">
                                <?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['nombre_sub']; ?>
</a> -->
                                <?php echo $this->_tpl_vars['enlaces_A'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['nombre_sub']; ?>

                                </div>
                            <?php endfor; endif; ?>
                        </div>
                        <?php endif; ?>
                    </li>
                	<?php endfor; endif; ?>
            </ul>
        </div>
        
        <div id="topbanner">
        	<div class="box_skitter box_skitter_large">  
			<ul>
            	<?php if ($this->_tpl_vars['banner'] != ""): ?>
                	<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['banner']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
					<li>
						<a <?php if ($this->_tpl_vars['banner'][$this->_sections['i']['index']]['vinculo_ban'] == ""): ?> href="#" <?php else: ?> href="<?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['vinculo_ban']; ?>
"<?php endif; ?>>
                        <img alt="<?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['etiqueta_ban']; ?>
" src="/imagenes/banner/<?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['url_ban']; ?>
" class="<?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['efecto_ban']; ?>
" /></a>
<div class="label_text">
							<p><?php echo $this->_tpl_vars['banner'][$this->_sections['i']['index']]['etiqueta_ban']; ?>
</p>
					  </div>
					</li>
                    <?php endfor; endif; ?>
                <?php else: ?>
                	<li>
                        <a href="#" title="Sigo Venezuela">
                        <img alt="Sigo Venezuela" src="/imagenes/banner_principal.png" /></a>
    <div class="label_text">
                            <p class="etiqueta">Sigo Venezuela</p>
                        </div>
                    </li>
                <?php endif; ?>
			  </ul>
        	</div>
        </div>
    </div>
  <div id="botonera_B">
   	<div id="enlaces">
    	<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_B']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
    		<div class="boton_B"><a href="contenido.php?cont=<?php echo $this->_tpl_vars['enlaces_B'][$this->_sections['i']['index']]['id_cat']; ?>
#next"><?php echo $this->_tpl_vars['enlaces_B'][$this->_sections['i']['index']]['nombre_cat']; ?>
</a></div>
        <?php endfor; endif; ?>
    </div>
<div id="redes">
        	<div class="redes_icono">
            <a href="http://www.facebook.com/SigoVenezuela" target="_blank" title="Siguenos es Facebook"><img src="/imagenes/icono_face.png" width="48" height="48" border="0" class="opacidad" /></a></div>
            <div class="redes_icono">
            <a href="http://twitter.com/sigosa" target="_blank" title="Siguenos es Twitter"><img src="/imagenes/icono_twitter.png" width="48" height="48" border="0" class="opacidad" /></a></div>
            <div class="redes_icono">
        <a href="http://www.youtube.com/SigoVenezuela" target="_blank" title="Siguenos es Youtube"><img src="/imagenes/icono_youtube.png" width="48" height="48" border="0" class="opacidad" /></a></div>
        </div> 
  </div>
  <a id="next"></a>
  <!-- InstanceBeginEditable name="contenido" -->
    <div id="secciones">
    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['contenido']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
    	<div class="panelitos">
       	  <div class="simbolo">
          	<a href="/contenido_detalle.php?id=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_con']; ?>
&cont=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_cat']; ?>
" title="<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['nombre_image']; ?>
">
            <img class="opacidad" src="/imagenes/<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['directorio_image']; ?>
"  width="93" /></a></div>
          <div class="titulo_detalle">
          	<a class="enlace" href="/contenido_detalle.php?id=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_con']; ?>
&cont=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_cat']; ?>
" title="<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['nombre_con']; ?>
"><?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['nombre_con']; ?>
</a></div>
          <div class="caja_detalles"><?php echo ((is_array($_tmp=$this->_tpl_vars['contenido'][$this->_sections['i']['index']]['contenido_con'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 80, "(...)") : smarty_modifier_truncate($_tmp, 80, "(...)")); ?>
 
            <a class="leermas" href="/contenido_detalle.php?id=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_con']; ?>
&cont=<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['id_cat']; ?>
" title="<?php echo $this->_tpl_vars['contenido'][$this->_sections['i']['index']]['nombre_con']; ?>
">Leer m&aacute;s &raquo;</a></div>
        </div>
    <?php endfor; endif; ?> 
    </div>
    <div id="paneles">   
      <div id="publicidad">
      		<div id="slidingFeatures"> 
                    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['publicidad']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <div title="<?php echo $this->_tpl_vars['publicidad'][$this->_sections['i']['index']]['nombre_dir']; ?>
">
                    <a title="<?php echo $this->_tpl_vars['publicidad'][$this->_sections['i']['index']]['nombre_dir']; ?>
" href="<?php echo $this->_tpl_vars['publicidad'][$this->_sections['i']['index']]['url_dir']; ?>
" target="_blank">
                        <img border="0" width="360" height="356" src="/imagenes/publicidad/<?php echo $this->_tpl_vars['publicidad'][$this->_sections['i']['index']]['directorio_dir']; ?>
" />
                    </a>
                    </div>
                    <?php endfor; endif; ?>
    			</div>
                <?php echo '
                <script type="text/javascript">		
                    $(document).ready(function(){ $(\'#slidingFeatures\').jshowoff({
                        effect: \'fade\',
                        controls: false,
                        links: false,
                        cssClass: \'basicFeatures\',
                        hoverPause: true,
                        speed : 6000,
                        changespeed : 500
                    }); });
                </script>
                '; ?>

      </div>
      <div id="publicidad2">
      	<div id="slidingFeatures2"> 
                    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['publicidad2']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <div title="<?php echo $this->_tpl_vars['publicidad2'][$this->_sections['i']['index']]['nombre_dir']; ?>
">
                    <a title="<?php echo $this->_tpl_vars['publicidad2'][$this->_sections['i']['index']]['nombre_dir']; ?>
" href="<?php echo $this->_tpl_vars['publicidad2'][$this->_sections['i']['index']]['url_dir']; ?>
" target="_blank">
                        <img border="0" width="290" height="356" src="/imagenes/publicidad/<?php echo $this->_tpl_vars['publicidad2'][$this->_sections['i']['index']]['directorio_dir']; ?>
" />	
                    </a>
                    </div>
                    <?php endfor; endif; ?>
    			</div>
                <?php echo '
                <script type="text/javascript">		
                    $(document).ready(function(){ $(\'#slidingFeatures2\').jshowoff({
                        effect: \'fade\',
                        controls: false,
                        links: false,
                        cssClass: \'basicFeatures\',
                        hoverPause: true,
                        speed : 6000,
                        changespeed : 500
                    }); });
                </script>
                '; ?>

      </div>
      <div id="panel_noticia">
      		<ul id="mycarousel" class="jcarousel jcarousel-skin-tango">
                <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['noticias']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                <li>
                	<div class="modulo_noticia">
                    <div class="foto_noticia">
                    <a href="noticia_detalle.php?id=<?php echo $this->_tpl_vars['noticias'][$this->_sections['i']['index']]['id_not']; ?>
#next" title="Ver Detalles" >
             <img alt="<?php echo $this->_tpl_vars['noticias'][$this->_sections['i']['index']]['nombre_image']; ?>
" border="0" src="/imagenes/<?php echo $this->_tpl_vars['noticias'][$this->_sections['i']['index']]['directorio_image']; ?>
" width="80" class="fotos2 opacidad"  longdesc="<?php echo $this->_tpl_vars['noticias'][$this->_sections['i']['index']]['nombre_pro']; ?>
" /></a>
             		</div>
               	    <div class="titulo_noticia">
                    <a title="<?php echo $this->_tpl_vars['noticias'][$this->_sections['i']['index']]['titulo_not']; ?>
" href="noticia_detalle.php?id=<?php echo $this->_tpl_vars['noticias'][$this->_sections['i']['index']]['id_not']; ?>
#next"><?php echo ((is_array($_tmp=$this->_tpl_vars['noticias'][$this->_sections['i']['index']]['titulo_not'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 200, "..") : smarty_modifier_truncate($_tmp, 200, "..")); ?>
</a></div>
                    <div class="vermas_noticia"><a title="<?php echo $this->_tpl_vars['noticias'][$this->_sections['i']['index']]['titulo_not']; ?>
" href="noticia_detalle.php?id=<?php echo $this->_tpl_vars['noticias'][$this->_sections['i']['index']]['id_not']; ?>
#next">ver más >></a></div>
                   </div>
              </li>
           	    <?php endfor; endif; ?>
                </ul>
      </div>
      </div>
  <!-- InstanceEndEditable -->
  <div id="menu_categorias">
  <div id="redes2">
   		<div class="redes_icono2"><a href="http://twitter.com/sigosa"><img src="/imagenes/logo_twitter_gris.png" width="46" height="46" border="0" id="Image1" onmouseover="MM_swapImage('Image1','','/imagenes/logo_twitter_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.facebook.com/SigoVenezuela"><img src="/imagenes/logo_face_gris.png" width="46" height="46" border="0" id="Image2" onmouseover="MM_swapImage('Image2','','/imagenes/logo_facebook_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
        <div class="redes_icono2"> <a href="http://www.youtube.com/SigoVenezuela"><img src="/imagenes/logo_youtube_gris.png" width="46" height="46" border="0" id="Image3" onmouseover="MM_swapImage('Image3','','/imagenes/logo_youtube_color.png',1)" onmouseout="MM_swapImgRestore()" /></a></div>
</div>
	
    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_C']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<div class="modulo">
            <div><a href="#" title="<?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['nombre_cat']; ?>
">
            <span class="titulo4"><?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['nombre_cat']; ?>
</span></a></div>
            <?php if ($this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces'] != ""): ?>
                <?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
                <div <?php if ($this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['id_cat'] == '16'): ?>class="submodulos2"<?php else: ?>class="submodulos"<?php endif; ?>>
                <a href="contenido_sub.php?cont=<?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['id_cat']; ?>
&sub=<?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['id_sub']; ?>
#next" title="<?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['etiqueta_sub']; ?>
" class="enlace_top">&bull; <?php echo $this->_tpl_vars['enlaces_C'][$this->_sections['i']['index']]['enlaces'][$this->_sections['j']['index']]['nombre_sub']; ?>
</a></div>
                <?php endfor; endif; ?>
            <?php endif; ?>
        </div>
     <?php endfor; endif; ?>
	
  </div>
</div>
<div class="clear"></div>

<div id="panel_pie">
    <div id="pie">
    <div id="botonera">
    	<div class="boton"><a href="index.php">Home</a></div>
        <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['enlaces_B']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
            <div class="boton"><a href="contenido.php?cont=<?php echo $this->_tpl_vars['enlaces_B'][$this->_sections['i']['index']]['id_cat']; ?>
#next"><?php echo $this->_tpl_vars['enlaces_B'][$this->_sections['i']['index']]['nombre_cat']; ?>
</a></div>
        <?php endfor; endif; ?>
        <div class="boton"><a title="Contactos"  href="contacto.php">Cont&aacute;ctanos</a></div>
        <div class="boton_less">&nbsp;</div>
    </div>
    <div id="logominimo"><img src="/imagenes/loguito.png" width="50" height="45" /></div>
    <div id="copy"> | SIGO S.A. &reg; 2016 RIF:J-08003048-6</div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>