<?php
//Iniciar sesión
session_start();

//Configure Page headers
header('P3P: CP="CAO PSA OUR"');
header("Cache-control: private");

// Configure Error Displaying
error_reporting(E_ERROR | E_WARNING | E_PARSE);

//Load Basic Configuration, Database & General Rutines
include_once "../admin/lib_php/config.php";          // Constantes Globales
include_once "../admin/lib_php/general.php";         // Funciones varias

?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <meta name="title" content="SIGO - Compra Rapida"/>
  <!-- <meta name="description" content="Desde aqui puedes elegir los productos que desees y luego pasar a retirar; o si prefieres, te lo llevamos a tu domicilio."/> -->
  <meta property="og:description" content="Desde aquí, puedes elegir los productos que desees comprar, y luego pasar a retirar por el Supermercado Sigo Costazul."/>
  <meta property="og:image" content="http://www.sigo.com.ve/img/sigo.webp"/>
  <!-- <meta property="og:image" content="http://www.sigo.com.ve/img/sigo.png"> -->

  <title> Compras Rapidas SIGO</title>

  <!-- <link rel="icon" href=" " type="image/x-icon"/> -->
  <link rel="icon" href="http://www.sigo.com.ve/favicon.ico" type="image/x-icon"/>
  <!-- <link rel="shortcut icon" href="http://www.sigo.com.ve/favicon.ico" type="image/x-icon"/> -->

  <style type="text/css">
    #iFrameForm {
			width: 100%;
      height: 100%;
      position: absolute;
    }

    body {
      overflow: hidden !important;
    }

  </style>
</head>

<body>

  <!-- begin: whataform -->
  <div class="row" id="rowForm">
    <div class="col s12 m12 l12" id="colForm">
      <iframe id="iFrameForm" src="https://whataform.com/f/ec6fefde305"></iframe>
    </div>
  </div>
  <!-- end: whataform -->

  </body>
</html>
