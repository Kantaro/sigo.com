<?php /* Smarty version 2.6.9, created on 2016-06-13 15:49:24
         compiled from admin/contenido/formulario.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/plantilla_admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Sigo S.A. - Panel Administrativo</title>
<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/imagenes/icono.ico"> 
<script type="text/javascript" language="javascript" src="/js/validar.js"></script>
<script src="/Scripts/swfobject_modified.js" type="text/javascript"></script>
<!-- InstanceBeginEditable name="head" -->
<?php echo '
<link rel="stylesheet" type="text/css" media="all" href="/calendario/calendar-blue.css" title="blue" />
<script type="text/javascript" src="/calendario/calendar.js"></script>
<script type="text/javascript" src="/calendario/lang/calendar-en.js"></script>
<script type="text/javascript" src="/calendario/calendar-setup.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/ckfinder/ckfinder.js"></script>
<script type="text/javascript">
	window.onload = function()
	{
		var editor = CKEDITOR.replace( \'contenido\',
			{
		toolbar :
			[
            		[\'Font\',\'FontSize\',\'TextColor\',\'RemoveFormat\',\'-\',\'Table\',\'Image\',\'Source\',\'Templates\' ],\'/\',

            		[\'Bold\', \'Italic\',\'Underline\', \'-\', \'NumberedList\', \'BulletedList\', \'-\', \'Link\',\'Unlink\'],[\'JustifyLeft\',\'JustifyCenter\',\'JustifyRight\',\'JustifyBlock\'],[\'Maximize\', \'ShowBlocks\']

        		]
 
    		}
		);
		CKFinder.setupCKEditor( editor, \'../../ckfinder/\' ) ;
	};
</script>

		<link rel="Stylesheet" media="screen" href="/hora/css/reset.css" />
        <link rel="Stylesheet" media="screen" href="/hora/css/styles.css" />
        <link rel="Stylesheet" media="screen" href="/hora/dist/themes/default/ui.core.css" />
        <link rel="Stylesheet" media="screen" href="/hora/dist/themes/default/ui.timepickr.css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
        <script type="text/javascript" src="/hora/js/jquery.utils.js"></script>
        <script type="text/javascript" src="/hora/js/jquery.strings.js"></script>
        <script type="text/javascript" src="/hora/js/jquery.anchorHandler.js"></script>
        <script type="text/javascript" src="/hora/js/jquery.ui.all.js"></script>
        <script type="text/javascript" src="/hora/src/ui.dropslide.js"></script>
        <script type="text/javascript" src="/hora/src/ui.timepickr.js"></script>


        <script type="text/javascript">
            $(function(){
                //$(\'#test-1\').timepickr({trigger: \'#trigger-test\'});
              $(\'#demo-1\').timepickr().focus();
              // temporary fix..
              $(\'.ui-dropslide ol:eq(0) li:first\').mouseover();
              // apply theme
              $(\'#demo-1\').next().addClass(\'dark\');
            });
        </script>
        
'; ?>

<!-- InstanceEndEditable -->

</head>  
<body>
<br />
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="marco">
  <tr>
    <td colspan="3" align="left" background="/imagenes/fondo_admin.jpg" class="subtituloWeb3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="42%"><img src="/imagenes/logo.jpg" width="400" height="122" /></td>
          <td width="56%" align="right" valign="middle" class="normalContenido2">Panel Central de Utilidades - <span class="subtituloWeb3">Usuario:</span> <?php echo $this->_tpl_vars['nombre']; ?>
 <?php echo $this->_tpl_vars['apellido']; ?>
 <img src="/imagenes/user.png" width="30" height="30" align="absmiddle" /><br />
            <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="597" height="48">
              <param name="movie" value="/swf/redes_hora.swf" />
              <param name="quality" value="high" />
              <param name="wmode" value="transparent" />
              <param name="swfversion" value="6.0.65.0" />
              <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don�t want users to see the prompt. -->
              <param name="expressinstall" value="/Scripts/expressInstall.swf" />
              <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
              <!--[if !IE]>-->
              <object type="application/x-shockwave-flash" data="/swf/redes_hora.swf" width="597" height="48">
                <!--<![endif]-->
                <param name="quality" value="high" />
                <param name="wmode" value="transparent" />
                <param name="swfversion" value="6.0.65.0" />
                <param name="expressinstall" value="/Scripts/expressInstall.swf" />
                <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                <div>
                  <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                  <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                </div>
                <!--[if !IE]>-->
              </object>
              <!--<![endif]-->
          </object></td>
          <td width="2%" align="right" valign="middle" class="normalContenido2">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
 
  <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
    <td colspan="3"><!-- InstanceBeginEditable name="contenido" -->
      <form action="" method="post" name="form1" id="form1" onsubmit="MM_validateForm('nombre','','R','f_date_c','','R');return document.MM_returnValue">
        <table border="0" align="center" cellpadding="0" cellspacing="0" class="normal">
          <tr>
            <th align="center"><span class="titulo"><img src="/imagenes/cuadros.png" width="14" height="14" align="left" /></span><?php echo $this->_tpl_vars['accion']; ?>
<img src="/imagenes/cuadritos.png" width="37" height="11" align="right" /></th>
          </tr>
          <tr>
            <td align="right" class="subtituloWeb3"><table width="54%" border="0" align="center" cellpadding="0" cellspacing="0">
              <?php echo $this->_tpl_vars['mensaje']; ?>

                  <tr>
                    <td align="left" class="subtituloWeb3">T&iacute;tulo:</td>
                    <td align="left" class="normalContenido"><input name="nombre" type="text" class="normalContenido" id="nombre" value="<?php echo $this->_tpl_vars['nombres']; ?>
" size="71" maxlength="100" /> 
                    *</td>
                  </tr>
                <tr>
    <td align="left" class="subtituloWeb3">Enlace:</td>
    <td align="left" class="normalContenido">
    <select name="enlace" id="enlace" onchange="javascripts: document.form1.submit();">
              <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['listado']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                <option <?php if ($this->_tpl_vars['enlace'] == $this->_tpl_vars['listado'][$this->_sections['i']['index']]['nombre_cat'] || $this->_tpl_vars['enlace'] == $this->_tpl_vars['listado'][$this->_sections['i']['index']]['id_cat']): ?> selected='selected'<?php endif; ?> value="<?php echo $this->_tpl_vars['listado'][$this->_sections['i']['index']]['id_cat']; ?>
"><?php echo $this->_tpl_vars['listado'][$this->_sections['i']['index']]['nombre_cat']; ?>
</option> 
              <?php endfor; endif; ?>
              </select> 
      *</td>
  </tr>
  <tr>
    <td align="left" class="subtituloWeb3">Subenlace:</td>
    <td align="left" class="normalContenido"><select name="subenlace" id="subenlace">
    			<option value="1">Ninguno(Principal)</option>
              <?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['listado2']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>   
                <option <?php if ($this->_tpl_vars['subenlace'] == $this->_tpl_vars['listado2'][$this->_sections['j']['index']]['nombre_sub']): ?>selected='selected'<?php endif; ?> value="<?php echo $this->_tpl_vars['listado2'][$this->_sections['j']['index']]['id_sub']; ?>
"><?php echo $this->_tpl_vars['listado2'][$this->_sections['j']['index']]['nombre_sub']; ?>
</option>
              <?php endfor; endif; ?>
              </select></td>
  </tr>
  <tr>
    <td align="left" class="subtituloWeb3">Fecha:</td>
    <td align="left" class="normalContenido"><span class="normalContenido">
      <input name="fecha" type="text" class="normalContenido" id="f_date_c"   value="<?php echo $this->_tpl_vars['fecha']; ?>
" size="30" maxlength="50" readonly="readonly" />
      </span><img src="/calendario/img.gif" name="f_trigger_c" width="20" height="14" align="absmiddle" id="f_trigger_c" style="cursor: pointer; border: 1px solid #005B7D;" title="Seleccionador de Fecha" onmouseover="this.style.background='#005B7D';" onmouseout="this.style.background=''" /> *</td>
  </tr>
  <tr>
    <td align="left" valign="top" class="subtituloWeb3">Hora:</td>
    <td align="left" class="normalContenido">
    	<div id="splash">
            <div id="demo">
                <div id="d-demo-wrapper-1" class="demo-wrapper">
                    <input id="demo-1" type="text" value="<?php echo $this->_tpl_vars['hora']; ?>
" class="demo" name="hora" readonly="readonly">
                </div>
            </div>
            
        </div>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" class="subtituloWeb3">Claves:</td>
    <td align="left" class="normalContenido"><textarea name="claves" cols="70" rows="4" class="normalContenido" id="claves"><?php echo $this->_tpl_vars['claves']; ?>
</textarea></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="subtituloWeb3">Contenido:</td>
    <td align="left" class="normalContenido"><textarea name="contenido" cols="70" rows="6" class="normalContenido" id="contenido"  wrap="physical"><?php echo $this->_tpl_vars['contenido']; ?>
</textarea> 
      *</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left" class="normalContenido"><input name="envio" type="submit" class="componentes" id="button" onclick="javascript: return validarcontenido('&iquest;Est&aacute; seguro que desea guardar?');" value="Guardar" />
      &nbsp;
      <input type="button" name="Submit3"  class="normalContenido" value="Cancelar" onclick="javascripts: location.href='/admin/contenido/'" /> 
      (*) Datos Obligatorios</td>
  </tr>
            </table></td>
          </tr>
        </table>
      </form>
      <?php echo '
      <script type=\'text/javascript\'>
	function catcalc(cal) {
		var date = cal.date;
		var time = date.getTime()
		var field = document.getElementById(\'f_calcdate\');
		if (field == cal.params.inputField) {
			field = document.getElementById(\'f_date_c\');
			time -= Date.WEEK;
		} else {
			time += Date.WEEK; 
		}
		var date2 = new Date(time);
		field.value = date2.print(\'%d/%m/%Y\');
	}
	Calendar.setup({
		inputField     :    \'f_date_c\',   
		ifFormat       :    \'%d/%m/%Y\',       // formato de fecha
		button         :    \'f_trigger_c\',
		singleClick    :    true
	});
</script>
      '; ?>

    <!-- InstanceEndEditable --></td>
  </tr>
    <tr>
    <td colspan="3" align="center" class="division"></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="division2"></td>
  </tr>
  <tr>
  <td width="25%" align="center"><a href="/admin/panel_central.php">Panel Central <img src="/imagenes/atras.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
    <td width="50%" align="center"><!-- InstanceBeginEditable name="insetar" -->&nbsp;<!-- InstanceEndEditable --></td>
    <td width="25%" align="center"><a href="/admin/cerrar_session.php">Cerrar Sesi&oacute;n <img src="/imagenes/cerrar.png" width="25" height="25" border="0" align="absmiddle" /></a></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"class="pie">
    Sigo S.A. / Copyright&copy; 2012 Todos los Derechos Reservados - Venezuela </td>
  </tr>
</table>
<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
</body>
<!-- InstanceEnd --></html>